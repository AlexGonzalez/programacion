import java.util.Scanner;

public class Activitat3i4_2NumeroMesGran {
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		float num = 0, num1 = 0;
		
		System.out.print("Introdueix el primer numero: ");
		
		num = reader.nextFloat();
		
		System.out.print("Introdueix el segon numero: ");
		
		num1 = reader.nextFloat();
		
		if(num < num1) {
			System.out.println("El segon numero es mes gran -->" + num1);
		}
		else if (num == num1){
			System.out.println("Son iguals");
		}
		else if (num > num1) {
			System.out.println("El primer numero es mes gran -->" + num);
		}	
		
		reader.close();
	}
}
