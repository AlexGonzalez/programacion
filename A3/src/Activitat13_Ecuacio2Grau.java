import java.util.Scanner;


public class Activitat13_Ecuacio2Grau {
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		double A = 0, B = 0, C = 0, D = 0, Res1 = 0, Res2 = 0;
		
		
		System.out.print("Introdueix el primer numero: ");
		A = reader.nextFloat();
		
		while (A == 0) { 
			System.out.print("El primer numero a de ser diferent de 0: ");
			A = reader.nextFloat();
		}
		
		System.out.print("Introdueix el segon numero: ");
		B = reader.nextFloat();
		System.out.print("Introdueix el tercer numero: ");
		C = reader.nextFloat();
		
		D = Math.pow(B, 2);
		C = Math.sqrt(4*A*C-D);
		
		
		
		Res1 = (-B+C)/(2*A);	
		Res2 = (-B-C)/(2*A);
		
		System.out.println("Els resultats son:" + Res1 + "," + Res2);
		
		reader.close();
	}

}
