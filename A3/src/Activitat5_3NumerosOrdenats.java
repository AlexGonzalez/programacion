import java.util.Scanner;

public class Activitat5_3NumerosOrdenats {

	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		float num = 0, num1 = 0, num2 = 0;
		
		System.out.print("Introdueix el primer numero: ");
		
		num = reader.nextFloat();
		
		System.out.print("Introdueix el segon numero: ");
		
		num1 = reader.nextFloat();
		
		System.out.print("Introdueix el tercer numero: ");
		
		num2 = reader.nextFloat();
		
		if(num > num1 && num > num2) {
			System.out.println("El numero mes gran es -->" + num);
		}
		else if (num1 > num && num1 > num2) {
			System.out.println("El numero mes gran es -->" + num1);
		}
		else if (num2 > num1 && num2 > num) {
			System.out.println("El numero mes gran es -->" + num2);
		}	
		else if (num2 == num1) {
			if(num < num2) {
				System.out.println("El segon i el tercer numero son iguals i son mes gran que el primer --> " + num2 );
			}
			else if (num > num2) {
				System.out.println("El numero mes gran es -->" + num);
			}
			else {
				System.out.println("Tots son iguals");
			}
		}	
		else if (num == num1) {
			if (num > num2) {
				System.out.println("El segon i el primer numero son iguals i son mes gran que el tercer --> " + num );
			}
			else if (num < num2) {
				System.out.println("El numero mes gran es -->" + num2);
			}
		}	
		else if (num2 == num) {
			if(num1 > num2) {
				System.out.println("El numero mes gran es -->" + num1);
			}
			else {
				System.out.println("El tercer i el primer numero son iguals i mes gran que el segon --> " + num );
			}
		}	
		else {
			System.out.println("Tots son iguals");
		}
		
		reader.close();
	}
}	
