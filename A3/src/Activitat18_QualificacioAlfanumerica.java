import java.util.Scanner;

public class Activitat18_QualificacioAlfanumerica {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		String Nota;
		System.out.print("Escriu la qualificacion alfanumerica amb una lletra: ");
		Nota = reader.nextLine();
		switch (Nota.toLowerCase()) {

		case "m":
			System.out.println("Molt Deficient 1.5");
			break;
		case "i":
			System.out.println("Insuficient 4.0");
			break;
		case "s":
			System.out.println("Suficient 5.5");
			break;
		case "b":
			System.out.println("Be 6.5");
			break;
		case "n":
			System.out.println("Notable 8.0");
			break;
		case "e":
			System.out.println("Excel.lent 9.5");
			break;
		default:
			System.out.println("Error");
		}

		reader.close();
	}
}
