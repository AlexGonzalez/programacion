import java.util.Scanner;

public class Activitat10_2Numeros {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float num = 0, num1 = 0, modul = 0;

		System.out.print("Introdueix el primer numero: ");
		num = reader.nextFloat();
		System.out.print("Introdueix el segon numero: ");
		num1 = reader.nextFloat();

		if (num > num1) {
			modul = num % num1;
			if (modul == 0) {
				System.out.println(num1 + " Es divisor de " + num);
			} else {
				System.out.println("No son divisors");
			}
		} else if (num < num1) {
			modul = num1 % num;
			if (modul == 0) {
				System.out.println(num + " Es divisor de " + num1);
			} else {
				System.out.println("No son divisors");
			}
		} else {
			System.out.println("Son iguals");
		}
		reader.close();
	}
}
