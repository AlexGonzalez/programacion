import java.util.Scanner;

public class Activitat7_2Numeros {
	
public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		float num = 0, num1 = 0, sum = 0,rest = 0,multi = 0,div = 0;
		
		System.out.print("Introdueix el primer numero: ");
		
		num = reader.nextFloat();
		
		System.out.print("Introdueix el segon numero: ");
		
		num1 = reader.nextFloat();
		
		sum = num + num1;
		rest = num - num1;
		multi = num * num1;
		div = num / num1;
		
		System.out.println("Suma = " + sum + ", Resta = " + rest + ", Multiplicació = " + multi + ", 3Divisió = " + div);
		
		
		
		
		reader.close();	
	}

}
