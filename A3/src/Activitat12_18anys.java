import java.util.*;
import java.util.Scanner;

public class Activitat12_18anys {

	public static void main(String[] args) {
	
		Scanner reader = new Scanner(System.in);
		Calendar Data = Calendar.getInstance(); 
		
		int any = 0, mes = 0, dia =0, anyac = 0, mesac = 0, diaac = 0; 
		
		anyac = Data.get(Calendar.YEAR);
		mesac = Data.get(Calendar.MONTH);
		diaac = Data.get(Calendar.DAY_OF_MONTH);
		
		
		System.out.println(Data.getTime());
		System.out.println(mesac);
		System.out.print("Introdueix el teu any de naixement: ");
		any = reader.nextInt();
		System.out.print("Introdueix el teu mes de naixement(numero de mes): ");
		
		mes = reader.nextInt();
		while (mes > 12) {
			System.out.print("Aquest mes no exiteix torna a introduir el teu mes de naixement(numero de mes): ");
			mes = reader.nextInt();
		}
		System.out.print("Introdueix el teu dia de naixement: ");
		dia = reader.nextInt();
		while (dia > 31) {
			System.out.print("Aquest dia no exiteix torna a introduir el teu dia de naixement: ");
			dia = reader.nextInt();
		}
		
		any = anyac-any;
		mes = mesac-mes;
		dia = diaac-dia;
		
		System.out.println(any + ","+ mes + "," + dia);
		
		if (any > 18) {
			System.out.println("Ets major d'edat");
		}
		else if (any == 18 && mes > 0) {
			System.out.println("Ets major d'edat");
		}
		else if (any == 18 && mes == 0 && dia == 0) {
			System.out.println("Ets major d'edat");
		}
		else {
			System.out.println("No ets major d'edat");
		}		
		reader.close();

		}
}
