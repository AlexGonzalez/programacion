import java.util.Scanner;

public class Activitat16_Qualificacions {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float num = 0;

		System.out.print("Introdueix un numero: ");
		num = reader.nextFloat();

		if (num < 5) {
			System.out.println("Insuficient");
		} else if (num < 6) {
			System.out.println("Suficient");
		} else if (num < 7) {
			System.out.println("Be");
		} else if (num < 9) {
			System.out.println("Notable");
		} else if (num < 10) {
			System.out.println("Excel.lent");
		} else {
			System.out.println("Matricula d'honor");
		}

		reader.close();
	}
}
