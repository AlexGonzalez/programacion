import java.util.*;
import java.util.Scanner;

public class Activitat12_VersioOK {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		Calendar Data = Calendar.getInstance();

		int any = 0, mes = 0, dia = 0, anyac = 0, mesac = 0, diaac = 0;

		anyac = Data.get(Calendar.YEAR);
		mesac = Data.get(Calendar.MONTH);
		diaac = Data.get(Calendar.DAY_OF_MONTH);

		System.out.print("Introdueix el teu any de naixement: ");
		any = reader.nextInt();
		System.out.print("Introdueix el teu mes de naixement(numero de mes): ");
		mes = reader.nextInt();

		while (mes > 12 || mes <= 0) {
			System.out.println("Mes no valid, torna'l a escriure");
			mes = reader.nextInt();
		}

		switch (mes) {

		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 31) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 31, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			break;

		case 4:
		case 6:
		case 9:
		case 11:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 30) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 3, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			break;

		case 2:
			if (((any % 4 == 0) && !(any % 100 == 0)) || (any % 400 == 0)) {
				System.out.print("Introdueix el teu dia de naixement: ");
				dia = reader.nextInt();
				while (dia > 29) {
					System.out.println("El numero es incorrecte, ha d'estar entre 1 i 29, torna'l a escriure: ");
					dia = reader.nextInt();
				}
			} else {
				System.out.print("Introdueix el teu dia de naixement: ");
				dia = reader.nextInt();
				while (dia > 28) {
					System.out.println("El numero es incorrecte, ha d'estar entre 1 i 28, torna'l a escriure: ");
					dia = reader.nextInt();
				}
			}
			break;
		default:
			System.out.println("Mes no valid");

		}
		any = anyac - any;
		mes = mesac - mes + 1;
		dia = diaac - dia;

		if (any > 18) {
			System.out.println("Ets major d'edat");
		} else if (any == 18 && mes > 0) {
			System.out.println("Ets major d'edat");
		} else if (any == 18 && mes == 0 && dia == 0) {
			System.out.println("Ets major d'edat");
		} else {
			System.out.println("No ets major d'edat");
		}

		reader.close();
	}
}