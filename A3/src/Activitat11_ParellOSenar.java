import java.util.Scanner;

public class Activitat11_ParellOSenar {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float num = 0, modul = 0;

		System.out.print("Introdueix un numero: ");

		num = reader.nextFloat();
		modul = num % 2;

		if (modul == 0) {
			System.out.println("El numero es parell");
		} else {
			System.out.println("El numero es senar");
		}

		reader.close();

	}

}
