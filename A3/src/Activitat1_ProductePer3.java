import java.util.Scanner; // TODO

public class Activitat1_ProductePer3 {

	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		float num = 0;
		
		System.out.print("Introdueix un numero: ");
		
		num = reader.nextFloat();
		num = num * 3;
		
		System.out.println("El numero multiplicat per 3 es: " + num);
		
		reader.close();

		}

}
