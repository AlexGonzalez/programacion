import java.util.Scanner;

public class Activitat6_Mostrar3Numeros {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float num = 0, num1 = 0, num2 = 0;
		String[] petit = new String[100], mitja = new String[100], gran = new String[100];

		System.out.print("Introdueix el primer numero: ");
		num = reader.nextFloat();
		System.out.print("Introdueix el segon numero: ");
		num1 = reader.nextFloat();
		System.out.print("Introdueix el tercer numero: ");
		num2 = reader.nextFloat();

		if (num2 == num1) {
			if (num < num2) {
				//gran[0] = num2; 
			//	mitja[0] = num1; 
				//petit[0] = num; 
				System.out.println(num2 + "," + num1 + "," + num);
				
			} else if (num > num2) {
				System.out.println(num + "," + num2 + "," + num1);
			} else {
				System.out.println("Tots son iguals");
			}
		} else if (num == num1) {
			if (num > num2) {
				System.out.println(num + "," + num1 + "," + num2);
			} else {
				System.out.println(num2 + "," + num + "," + num1);
			}
		} else if (num2 == num) {
			if (num1 > num2) {
				System.out.println(num1 + "," + num + "," + num2);
			} else {
				System.out.println(num + "," + num2 + "," + num1);
			}
		}
		if (num > num1 && num > num2) {
			if (num1 > num2) {
				System.out.println(num + "," + num1 + "," + num2);
			} else if (num2 > num1) {
				System.out.println(num + "," + num2 + "," + num1);
			}
		} else if (num1 > num && num1 > num2) {
			if (num > num2) {
				System.out.println(num1 + "," + num + "," + num2);
			} else if (num2 > num) {
				System.out.println(num1 + "," + num2 + "," + num);
			}
		} else if (num2 > num1 && num2 > num) {
			if (num1 > num) {
				System.out.println(num2 + "," + num1 + "," + num);
			} else if (num > num1) {
				System.out.println(num2 + "," + num + "," + num1);
			}
		}
		reader.close();

	}
}