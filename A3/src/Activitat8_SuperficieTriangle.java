import java.util.Scanner;

public class Activitat8_SuperficieTriangle {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float altura = 0, base = 0, superficie = 0;
		System.out.println("Posa l'altura del triangle: ");
		altura = reader.nextFloat();
		System.out.println("Posa la base del triangle: ");
		base = reader.nextFloat();

		superficie = base * altura / 2;

		System.out.println("La superficie es: " + superficie);

		reader.close();
	}

}
