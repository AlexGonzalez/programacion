import java.util.*;
import java.util.Scanner;

public class Activitat12_MesLletra {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		Calendar Data = Calendar.getInstance();

		int any = 0, dia = 0, anyac = 0, mesac = 0, diaac = 0, mesn = 0;
		String mes;

		anyac = Data.get(Calendar.YEAR);
		mesac = Data.get(Calendar.MONTH);
		diaac = Data.get(Calendar.DAY_OF_MONTH);

		System.out.print("Introdueix el teu any de naixement: ");
		any = reader.nextInt();

		System.out.print("Introdueix el teu mes de naixement(catala): ");
		mes = reader.next();

		switch (mes.toLowerCase()) {
		case "gener":
			mesn = 1;
			break;
		case "febrer":
			mesn = 2;
			break;
		case "mar�":
			mesn = 3;
			break;
		case "abril":
			mesn = 4;
			break;
		case "maig":
			mesn = 5;
			break;
		case "juny":
			mesn = 6;
			break;
		case "juliol":
			mesn = 7;
			break;
		case "agost":
			mesn = 8;
			break;
		case "septembre":
			mesn = 9;
			break;
		case "octubre":
			mesn = 10;
			break;
		case "novembre":
			mesn = 11;
			break;
		case "decembre":
			mesn = 12;
			break;
		}

		switch (mesn) {

		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 31) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 31, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			break;

		case 4:
		case 6:
		case 9:
		case 11:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 30) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 3, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			break;

		case 2:
			if (((any % 4 == 0) && !(any % 100 == 0)) || (any % 400 == 0)) {
				System.out.print("Introdueix el teu dia de naixement: ");
				dia = reader.nextInt();
				while (dia > 29) {
					System.out.println("El numero es incorrecte, ha d'estar entre 1 i 29, torna'l a escriure: ");
					dia = reader.nextInt();
				}
			} else {
				System.out.print("Introdueix el teu dia de naixement: ");
				dia = reader.nextInt();
				while (dia > 28) {
					System.out.println("El numero es incorrecte, ha d'estar entre 1 i 28, torna'l a escriure: ");
					dia = reader.nextInt();
				}
			}
			break;
		default:
			System.out.println("Mes no valid");

		}
		any = anyac - any;
		mesn = mesac - mesn + 1;
		dia = diaac - dia;

		if (any > 18) {
			System.out.println("Ets major d'edat");
		} else if (any == 18 && mesn > 0) {
			System.out.println("Ets major d'edat");
		} else if (any == 18 && mesn == 0 && dia == 0) {
			System.out.println("Ets major d'edat");
		} else {
			System.out.println("No ets major d'edat");
		}

		reader.close();
	}
}
