import java.util.Scanner;

public class Activitat20_DiaSeguent {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int any = 0, mes = 0, dia = 0;

		System.out.print("Introdueix l'any : ");
		any = reader.nextInt();
		System.out.print("Introdueix el mes (numero de mes): ");
		mes = reader.nextInt();

		while (mes > 12 || mes <= 0) {
			System.out.println("Mes no valid, torna'l a escriure");
			mes = reader.nextInt();
		}

		switch (mes) {

		case 1: case 3: case 5:	case 7:	case 8: case 10: case 12:
			System.out.print("Introdueix el teu dia: ");
			dia = reader.nextInt();
			while (dia > 31 || dia <= 0) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 31, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			if (dia == 31) {
				if (mes == 12) {
					dia = 1;
					mes = 1;
					any = any + 1;
				}
				else {
					dia = 1;
					mes = mes + 1;
				}
			}	
			else {
				dia = dia + 1;
			}					
			break;
			
		case 4: case 6: case 9: case 11:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 30 || dia <= 0) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 3, torna'l a escriure: ");
				dia = reader.nextInt();
			}	
				if (dia == 30) {
					dia = 1;
					mes = mes + 1;
					}	
				else {
					dia = dia + 1;
				}	
			
			break;
		case 2:	
			if (dia == 28) {
				dia = 1;
				mes = mes + 1;
				}	
			else {
				dia = dia + 1;
			}
		}
		System.out.println("Dia seguent,Dia: " + dia + ", Mes:" + mes + ", Any:" + any );
		reader.close();
	}
}