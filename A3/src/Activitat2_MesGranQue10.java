import java.util.Scanner; // TODO



public class Activitat2_MesGranQue10 {
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		float num = 0;
		
		System.out.print("Introdueix un numero: ");
		
		num = reader.nextFloat();
		
		if(num < 10) {
			System.out.println("El numero es mes petit que 10");
		}
		else if (num == 10) {
			System.out.println("Son igual");
		}
		else if (num > 10) {
			System.out.println("El numero es mes gran que 10");
		}
		
		reader.close();
	}
}

