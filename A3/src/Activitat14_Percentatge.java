import java.util.Scanner;

public class Activitat14_Percentatge {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float preu = 0;
		double descompte = 0, preuf = 0;
		System.out.print("Introdueix un preu: ");

		preu = reader.nextFloat();

		if (preu > 500) {
			preuf = preu * 0.9;
			descompte = preu - preuf;
			System.out.println("Preu final: " + preuf + ", Descompte: " + descompte);
		} else {
			preuf = preu * 0.95;
			descompte = preu - preuf;
			System.out.println("Preu final: " + preuf + ", Descompte: " + descompte);
		}

		reader.close();
	}

}
