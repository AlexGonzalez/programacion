import java.util.Scanner;

public class Activitat17_PreuTarifa {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		double Preuf = 0, Preui = 0, Descompte = 0;

		System.out.print("Introdueix el preu pagat: ");
		Preuf = reader.nextFloat();

		System.out.print("Introdueix el preu sense rebaixa: ");
		Preui = reader.nextFloat();

		Preuf = Preui - Preuf;
		Descompte = Preuf / Preui * 100;

		System.out.println("El descompte es del: " + Descompte + "%");

		reader.close();
	}
}
