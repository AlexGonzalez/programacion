
import java.util.Scanner;

public class Activitat19_horoscopo {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int any = 0, mes = 0, dia = 0;
		String[] Horoscopo = new String[100];

		System.out.print("Introdueix el teu any de naixement: ");
		any = reader.nextInt();
		System.out.print("Introdueix el teu mes de naixement(numero de mes): ");
		mes = reader.nextInt();

		while (mes > 12 || mes <= 0) {
			System.out.println("Mes no valid, torna'l a escriure");
			mes = reader.nextInt();
		}

		switch (mes) {

		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 31 || dia <= 0) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 31, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			break;

		case 4:
		case 6:
		case 9:
		case 11:
			System.out.print("Introdueix el teu dia de naixement: ");
			dia = reader.nextInt();
			while (dia > 30 || dia <= 0) {
				System.out.println("El numero es incorrecte, ha d'estar entre 1 i 3, torna'l a escriure: ");
				dia = reader.nextInt();
			}
			break;

		case 2:
			if (((any % 4 == 0) && !(any % 100 == 0)) || (any % 400 == 0)) {
				System.out.print("Introdueix el teu dia de naixement: ");
				dia = reader.nextInt();
				while (dia > 29) {
					System.out.println("El numero es incorrecte, ha d'estar entre 1 i 29, torna'l a escriure: ");
					dia = reader.nextInt();
				}
			} else {
				System.out.print("Introdueix el teu dia de naixement: ");
				dia = reader.nextInt();
				while (dia > 28) {
					System.out.println("El numero es incorrecte, ha d'estar entre 1 i 28, torna'l a escriure: ");
					dia = reader.nextInt();
				}
			}
			break;
		default:
			System.out.println("Mes no valid");

		}

		switch (mes) {

		case 1:
			if (dia <= 20) {
				Horoscopo[0] = "Capricorn";
				// System.out.println("Ets Capricorn");
			} else {
				Horoscopo[0] = "Acuario";
			}
			break;
		case 2:
			if (dia <= 19) {
				Horoscopo[0] = "Acuario";
			} else {
				Horoscopo[0] = "Piscis";
			}
			break;
		case 3:
			if (dia <= 20) {
				Horoscopo[0] = "Piscis";
			} else {
				Horoscopo[0] = "Aries";
			}
			break;
		case 4:
			if (dia <= 20) {
				Horoscopo[0] = "Aries";
			} else {
				Horoscopo[0] = "Tauro";
			}
			break;
		case 5:
			if (dia <= 21) {
				Horoscopo[0] = "Tauro";
			} else {
				Horoscopo[0] = "Geminis";
			}
			break;
		case 6:
			if (dia <= 21) {
				Horoscopo[0] = "Geminis";
			} else {
				Horoscopo[0] = "Cancer";
			}
			break;
		case 7:
			if (dia <= 23) {
				Horoscopo[0] = "Cancer";
			} else {
				Horoscopo[0] = "Leo";
			}
			break;
		case 8:
			if (dia <= 23) {
				Horoscopo[0] = "Leo";
			} else {
				Horoscopo[0] = "Virgo";
			}
			break;
		case 9:
			if (dia <= 23) {
				Horoscopo[0] = "Virgo";
			} else {
				Horoscopo[0] = "Libra";
			}
			break;
		case 10:
			if (dia <= 23) {
				Horoscopo[0] = "Libra";
			} else {
				Horoscopo[0] = "Escorpio";
			}
			break;
		case 11:
			if (dia <= 22) {
				Horoscopo[0] = "Escorpio";
			} else {
				Horoscopo[0] = "Sagitario";
			}
			break;
		case 12:
			if (dia <= 21) {
				Horoscopo[0] = "Sagitario";
			} else {
				Horoscopo[0] = "Capricornio";
			}
			break;
		}
		System.out.println("Ets " + Horoscopo[0]);

		reader.close();
	}
}