import java.util.Scanner;

public class Activitat9_Multiple2o3 {
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		int num = 0, restant2 = 0, restant3 = 0;
		
		System.out.print("Introdueix un numero: ");
		
		num = reader.nextInt();
		
		restant2 = num%2;
		restant3 = num%3;
				
		if (restant2 == 0 && restant3 == 0) {
			System.out.println("El numero es multiple de 2 i 3");
		}
		else if (restant2 == 0) {
			System.out.println("El numero es multiple de 2");
		}
		else if (restant3 == 0) {
			System.out.println("El numero es multiple de 3");		
		}
		else {
			System.out.println("No es multiple ni de 2 ni de 3");
		}
			
		reader.close();
	}	
}
