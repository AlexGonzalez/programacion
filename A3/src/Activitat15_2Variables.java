import java.util.Scanner;

public class Activitat15_2Variables {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		float A = 0, B = 0, C = 0;

		System.out.print("Introdueix un numero: ");
		A = reader.nextFloat();
		System.out.print("Introdueix un numero: ");
		B = reader.nextFloat();

		C = A;
		A = B;
		B = C;
		System.out.println(A + "," + B);

		reader.close();
	}
}
