

/*Institut Sabadell.
CFGS DAM M03 UF1
Autor: Rub�n Cosp
 */

import java.util.Scanner;

public class LA_SERP_CORREGIT {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);

		try {
			System.out.println("Escriu la longitud del tauler:");
			int m = reader.nextInt();
			System.out.println("Escriu l'al�ada del tauler:");
			int n = reader.nextInt();
	
			String[][] tauler = new String[n][m];
	
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					tauler[i][j] = "-";
				}
			}
			int x = (int)(Math.random() * n);
			int y = (int)(Math.random() * m);
			tauler[x][y] = "*";
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					System.out.print(tauler[i][j]);
					System.out.print(" ");
				}
				System.out.println();
			}
			System.out.println("'a': una posici� cap a dalt\n'b': una posici� cap a baix\n'd': una posici� cap a la dreta\n'e': una posici� cap a l'esquerra");
			System.out.println("Quins moviments vols fer?");
			char[] movs = reader.next().toCharArray();
			
			for(int i = 0;i < movs.length;i++) {
				switch(movs[i]) {
				case 'a':
					x--;
					if(x >= 0) {
						tauler[x][y] = "*";
					}else {
						System.out.println("[ERROR] Amb aquestes indicacions la serp surt del tauler.");
					}
					break;
				case 'b':
					x++;
					if(x < tauler.length) {
						tauler[x][y] = "*";
					}else {
						System.out.println("[ERROR] Amb aquestes indicacions la serp surt del tauler.");
					}
					break;
				case 'd':
					y++;
					if(y < tauler[x].length) {
						tauler[x][y] = "*";
					}else {
						System.out.println("[ERROR] Amb aquestes indicacions la serp surt del tauler.");
					}
					break;
				case 'e':
					y--;
					if(y >= 0) {
						tauler[x][y] = "*";
					}else {
						System.out.println("[ERROR] Amb aquestes indicacions la serp surt del tauler.");
					}
					break;
				}
			}
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					System.out.print(tauler[i][j]);
					System.out.print(" ");
				}
				System.out.println();
			}
		}catch(Exception e) {
			System.out.println("[ERROR] Escriu un n�mero enter.");
		}
		reader.close();
	}
}