
import java.util.Scanner;

/*Institut Sabadell.
CFGS DAM M03 UF1
Autor: Rub�n Cosp
 */

public class PRODUCTE_MATRIUS_CORREGIT {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Dimesions de la primera matriu:");
		System.out.print("Nombre de files: ");
		int y1 = reader.nextInt();
		System.out.print("Nombre de columnes: ");
		int x1 = reader.nextInt();
		int[][] mat1 = new int[y1][x1];
		
		System.out.println("Dimesions de la segona matriu:");
		System.out.print("Nombre de files: ");
		int y2 = reader.nextInt();
		System.out.print("Nombre de columnes: ");
		int x2 = reader.nextInt();
		int[][] mat2 = new int[y2][x2];
		
		if(x1 == y2) {
			for(int i = 0;i < y1;i++) {
				for(int j = 0;j < x1;j++) {
					mat1[i][j] = (int)(Math.random() * 10);
					System.out.print(mat1[i][j]);
					System.out.print(" ");
				}
				System.out.println();
			}
			System.out.println();
			for(int i = 0;i < y2;i++) {
				for(int j = 0;j < x2;j++) {
					mat2[i][j] = (int)(Math.random() * 10);
					System.out.print(mat2[i][j]);
					System.out.print(" ");
				}
				System.out.println();
			}
			System.out.println();
			int[][] mat3 = new int[y1][x2];
			for(int i = 0;i < y1;i++) {
				for(int j = 0;j < x2;j++) {
					for(int h = 0;h < y2;h++) {
						mat3[i][j] = mat3[i][j] + mat1[i][h]*mat2[h][j];
					}
					System.out.print(mat3[i][j]);
					System.out.print(" ");
				}
				System.out.println();
			}
		}else {
			System.out.println("Les dimensions de les matrius no coincideixen.");
		}
		reader.close();	
	}
}