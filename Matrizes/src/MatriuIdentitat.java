
import java.util.Scanner;
public class MatriuIdentitat {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Introdueix el n�mero de files");
		int dim = reader.nextInt(); 
		int matriu [][] = new int[dim][dim];
		boolean identitat = true;
		for(int i = 0;i < dim; i++) {
			for(int j = 0; j< dim; j++) {
				System.out.print("Introdueix l'element [" + i +"] ["+ j + "]");
				matriu[i][j] = reader.nextInt();
			}
		}
		int index = 0;	
		while (index < dim && identitat ==true) {
			if (matriu[index][index] != 1) {
				identitat = false;
			} else {
				index++;
			}
		}
		if (identitat == true) {
			System.out.print("SI");
		} else {
			System.out.print("NO");
		}		
		reader.close();
	}
}
