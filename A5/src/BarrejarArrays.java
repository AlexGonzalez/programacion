public class BarrejarArrays {
	public static void main(String[] args) {

		int[] arrayA = { 1, 3, 5, 7, 9, 11, 13, 15 };
		int[] arrayB = { 2, 4, 6, 8, 10, 12, 14, 16 };
		int[] arrayC = new int[arrayA.length + arrayB.length];
		int j = 0;
		for (int i = 0; i < arrayC.length; i = i + 2) {
			arrayC[i] = arrayA[j];
			j++;
		}
		j = 0;
		for (int i = 1; i < arrayC.length; i = i + 2) {
			arrayC[i] = arrayB[j];
			j++;
		}
		for (int i = 0; i < arrayC.length; i++) {
			System.out.print(arrayC[i] + ", ");
		}

	}
}