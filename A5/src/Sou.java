/*
 * @author Alex Gonzalez Reinoso
 * 
 */
import java.util.Scanner;

public class Sou {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int opcio = 0, empleat = 0, percent = 0;
		double brut[] = new double[10];
		double net[] = new double[10];
		double sou = 0, treure1 = 0, treure2 = 0, nousou = 0;
		boolean sortir = false;
		char sn;

		System.out.println("Modificació de sous de l'empresa");
		while (!sortir) {
			System.out.println("\t1.Introduir sous (10 empleats)");
			System.out.println("\t2.Calcular sous nets (salari)");
			System.out.println("\t3.Modificar sou brut");
			System.out.println("\t4.Pujar sous bruts");
			System.out.println("\t5.Sortir programa (5)");
			System.out.println("_______________________________________");
			System.out.print("\tEscull una opcio: ");
			opcio = reader.nextInt();
			while (opcio < 1 || opcio > 5) {
				System.out.print("Opcio no valida, torna a provar: ");
				opcio = reader.nextInt();
			}
			switch (opcio) {
			case 1:
				System.out.println("Escriu el sou dels 10 empleats");
				for (int i = 1; i < brut.length + 1; i++) {
					System.out.print("El sou del " + i + " empleat es: ");
					sou = reader.nextDouble();
					while (sou < 0 || sou > 5000) {
						System.out.print("El sou ha d'estar entre 0 i 5000, torna a provar");
						sou = reader.nextDouble();
					}
					brut[i - 1] = sou;
				}
				break;
			case 2:
				for (int j = 0; j < brut.length; j++) {
					treure1 = brut[j] * 0.06;
					if (brut[j] <= 700) {
						treure2 = brut[j] * 0.08;
					} else if (brut[j] > 700 && brut[j] <= 1100) {
						treure2 = brut[j] * 0.11;
					} else if (brut[j] > 1000 && brut[j] <= 1500) {
						treure2 = brut[j] * 0.13;
					} else if (brut[j] > 1500 && brut[j] <= 2100) {
						treure2 = brut[j] * 0.17;
					} else if (brut[j] > 2100 && brut[j] <= 3000) {
						treure2 = brut[j] * 0.20;
					} else {
						treure2 = brut[j] * 0.25;
					}
					net[j] = brut[j] - treure1 - treure2;
					treure1 = 0;
					treure2 = 0;
				}
				for (int i = 1; i < net.length + 1; i++) {
					System.out.println("Sou " + i + " empleat: " + net[i - 1]);
				}
				break;
			case 3:
				System.out.println("A quin empleat vols modficar el sou");
				empleat = reader.nextInt();
				while (empleat < 0 || empleat > 10) {
					System.out.print("No hi han mes de 10 empleats, torna a provar: ");
					empleat = reader.nextInt();
				}
				System.out.println("El seu sou brut es: " + brut[empleat - 1]);
				System.out.println("El vols modificar S/N");
				sn = reader.next().charAt(0);
				switch (sn) {
				case 's':
					System.out.println("Escriu el nuo sou brut: ");
					nousou = reader.nextDouble();
					while (nousou < 0 || nousou > 5000) {
						System.out.print("El sou ha d'estar entre 0 i 5000, torna a provar: ");
						nousou = reader.nextDouble();
					}
					brut[empleat - 1] = nousou;
					break;
				case 'n':
					break;
				}
				break;
			case 4:
				System.out.print("Quin percentage vols augmentar a tots els sous bruts: ");
				percent = reader.nextInt();
				while (percent < 0) {
					System.out.print("No pot ser negatiu, tornaa provar: ");
					percent = reader.nextInt();
				}
				for (int i = 0;i < brut.length ;i++) {
					brut[i] = brut[i] + brut[i]*(percent/100);
				}
				break;
			case 5:
				System.out.println("Fins la proxima.");
				sortir = true;
				break;
			} 
		}
		reader.close();
	}
}