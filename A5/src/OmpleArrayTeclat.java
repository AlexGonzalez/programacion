/*
 * @author Alex Gonzalez Reinoso
 * 
 */
import java.util.Scanner;

public class OmpleArrayTeclat {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int[] array = new int[20];	
		for (int i = 0; i < array.length; i++) {
			array[i] = reader.nextInt();			
		}
		for (int i = 0; i < array.length - 1; i++) {
			System.out.print(array[i] + ", ");
		}
		System.out.println(array[array.length - 1]);
		
		
		reader.close();
	}
}