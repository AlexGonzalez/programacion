/*
 * @author Alex Gonzalez Reinoso
 * 
 */
import java.util.Scanner;

/**
 * @author AlexGonzalezReinoso
 *
 */
public class Nivell1_1 {
	public static void main(String[] args) {
		int tamañ = 0, num = 0, cont = -1;
		Scanner reader = new Scanner(System.in);
		System.out.println("Tamañ de l'array (minim 3): ");
		tamañ = reader.nextInt();
		num = tamañ/3;
		int valor[]	= new int [tamañ];
		for (int i = 0; i < valor.length; i++) {
			valor[i] = (int) Math.random();
		}	
		for (int i = 0; cont-1 == num;i++) {
			if (i%3 == 0) {
			System.out.println(valor[i] + ", ");
			 cont++;
			}
		}	
		reader.close();		
	}	
}
