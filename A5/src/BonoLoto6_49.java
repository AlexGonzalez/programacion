/*
 * @author Alex Gonzalez Reinoso
 * 
 */
import java.util.Arrays;
import java.util.Scanner;

public class BonoLoto6_49 {
	
	public static void main(String[] args) {

		try (Scanner reader = new Scanner(System.in)) {
			int premi = 0, num = 0, cont = 0, contpremi = 0, pos = 0, i = 0, premicont = 0;
			int premiat[] = new int[6];
			int papereta[] = new int[6];
			boolean paperetavalida = false;
			System.out.print("Quants diners hi ha de premi: ");
			premi = reader.nextInt();
			while (premi < 0) {
				System.out.println("Ha de ser un numero enter");
				premi = reader.nextInt();
			}
			for (i = 0; contpremi <= 5; i++) {
				num = (int) (Math.random() * (49) + 1);
				cont = 0;
				for (int j = 0; j <= premiat.length - 1; j++) {
					if (num == premiat[j]) {
						cont++;
					}
				}
				if (cont == 0) {
					premiat[pos] = num;
					contpremi++;
					pos++;
				}
			}
			Arrays.sort(premiat);
			System.out.println("Escriu la teva papereta: ");
			for (i = 0; i < papereta.length; i++) {
				papereta[i] = reader.nextInt();
			}
			for (int j = 0; j < papereta.length; j++) {
				if (j > 0) {
					if (papereta[j] == papereta[j - 1]) {
						premicont++;
					}
				}
			}
			if (premicont == 0) {
				paperetavalida = true;
			}
			cont = 0;
			if (paperetavalida) {
				for (i = 0; i < papereta.length; i++) {
					for (int j = 0; j < premiat.length; j++) {
						if (premiat[j] == papereta[i]) {
							cont++;
						}
					}
				}
				switch (cont) {
				case 1:
					System.out.println("Has encertat 1 pero no tens premi.");
					break;
				case 2:
					System.out.println("Has encertat 2, felicitats has guanyat 5€.");
					break;
				case 3:
					System.out.println("Has encertat 3, felicitats has guanyat 10€.");
					break;
				case 4:
					System.out.println("Has encertat 4, felicitats has guanyat " + (premi * 0.04) + ".");
					break;
				case 5:
					System.out.println("Has encertat 5, felicitats has guanyat " + (premi * 0.05) + ".");
					break;
				case 6:
					System.out.println("Has encertat 6, felicitats has guanyat " + premi + ".");
					break;
				default:
					System.out.println("No has encertat cap.");
				}
			} else {
				System.out.println("Papereta invalida");
			}
			System.out.print("Papereta guanyadora: ");
			for (int j = 0; j < premiat.length; j++) {
				System.out.print(premiat[j] + " ");
			}
			
			reader.close();
		}
	}
}
