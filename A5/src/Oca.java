/*
 * @author Alex Gonzalez Reinoso
 * 
 */
import java.util.Scanner;

public class Oca {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		boolean sortir = false;
		char joc[] = new char[32];
		int caselles[] = new int [32];
		int posicio[] = new int [32];
		int opcio = 0, dau = 0, pos = 0;
		int casella1 = 0, casella2 = 0;
		
		
		
		

		while (!sortir) {
			System.out.println("\t1.Inicialitzar");
			System.out.println("\t2.Aparellar");
			System.out.println("\t3.Veure Joc");
			System.out.println("\t4.Llençar");
			System.out.println("\t0.Sortir programa (0)");
			System.out.println("_______________________________________");
			System.out.print("\tEscull una opcio: ");
			opcio = reader.nextInt();
			System.out.println("\n\n\n\n\n\n");

			switch (opcio) {
			case 1:
				for (int i = 1; i < joc.length + 1 ; i++) {
					caselles[i-1] = 0;
					posicio[i-1] = i;
				}
				joc[0] = 'X';
				System.out.println("\n\n\n");
				break;
			case 2:
				System.out.print("Quines caselles vols aparellar: ");
				casella1 = reader.nextInt();
				casella2 = reader.nextInt();
				if ((casella1 >= 2 && casella1 <= 31) && (casella2 >= 2 && casella2 <= 31)) {
			
					caselles[casella1-1] = casella2;
					caselles[casella2-1] = casella1;
				} else {
					System.out.println("Error, aquestes caselles no son compatibles.");
				}
				break;
			case 3:
				System.out.println("Posicio");
				for (int c : posicio) {
					System.out.print(c + "|");
				}
				System.out.println("\n");
				System.out.println("Joc");
				for (char c : joc) {
					System.out.print(c+ "|");
				}
				System.out.println("\n");
				System.out.println("Caselles");
				for (int c : caselles) {
					System.out.print(c+ "|");
				}
				System.out.println("\n");
				break;
			case 4:
				dau = (int) (Math.random() * (6) + 1);
				System.out.println("Has tret un: " + dau);
				pos = pos + dau;
				if (pos >= 32) {
					System.out.println("Joc finalitzat");
				}
				if (caselles[pos-1] != 0) {
					joc[pos] = 0;
					joc[caselles[pos-1]-1] = 'X'; 
				}				
				break;
			case 0:
				sortir = true;
				break;
			}
		}
		reader.close();
	}
}