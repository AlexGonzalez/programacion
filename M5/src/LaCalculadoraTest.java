import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LaCalculadoraTest {

	
	@Test
	public void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30,  res);
		// fail("Not yet implemented");
	}
	@Test
	public void testResta() {
		int res = LaCalculadora.resta(20, 20);
		assertEquals(12,  res);
		// fail("Not yet implemented");
	}
	@Test
	public void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(20, 3);
		assertEquals(54,  res);
		// fail("Not yet implemented");
	}
	@Test
	public void testDivisio() {
		int res = LaCalculadora.divisio(20, 20);
		assertEquals(1,  res);
		// fail("Not yet implemented");
	}

	@Test
	public void testDivideix() {
		int res = LaCalculadora.divideix(12,  0);
		assertEquals(1,  res);
		// fail("Not yet implemented");
	}
	@Test
	void testExpectedException() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			LaCalculadora.divideix(10,2);
		});		
	}
}

