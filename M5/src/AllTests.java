import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CalculadoraTestDivParam.class, CalculadoraTestMultParam.class, CalculadoraTestRestaParam.class,
		CalculadoraTestSumaParam.class })
public class AllTests {

}
