import java.util.Scanner;

/*
 * Qualificaciones : 
Has de fer un programa per ajudar-me a calcular les vostres qualificacions d’M03-UF1.
Primer de tot el programa preguntarà la quantitat d’alumnes del grup. 
Després preguntarà i guardarà els seus noms. 
A continuació es preguntaran les notes dels dos exàmens de cada alumne (1 punt) i amb elles es calcularan les qualificacions finals de la UF .
Finalment es mostraran la llista dels alumnes i les qualificacions finals i el programa acabarà. 
S’ha de tenir en compte que:
El nombre d’alumnes ha de ser enter, positiu i major que 0. Cal mostrar els missatges adequats si no es verifica. 
Totes les qualificacions han d’estar entre 0 i 10. Cal mostrar els missatges adequats si no es verifica. 
Suposarem que tots els alumnes es presenten als dos exàmens.
Les qualificacions es calculen de la forma següent:
La qualificació serà el 30% del primer examen més el 70% del segon sempre que la qualificació del primer sigui superior a la del segon.
Si la qualificació del segon examen és superior o igual a la del primer la qualificació serà el 10% del primer examen més el 90% de la del segon.
En qualsevol cas, si algun dels dos exàmens té una qualificació inferior a 2 la qualificació màxima serà 4.
No cal fer un servir un menú.
 */

public class Qualificacions {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int numero = 0;
		float numero2 = 0;
		String[] noms;
		float nota1[];
		float nota2[];
		float notaf[];
		int k = 0;

		while (numero <= 0) {
			try {
				System.out.println("Digui el numero desitjat: ");
				numero = reader.nextInt();
				if (numero <= 0)
					System.out.println("El numero ha de ser positius!");
			} catch (Exception e) {
				System.out.println("Ha de ser un numero enter!");
				reader.nextLine();
			}
		}

		noms = new String[numero];
		nota1 = new float[numero];
		nota2 = new float[numero];
		notaf = new float[numero];
		reader.nextLine();

		for (int i = 0; i < noms.length; i++) {
			System.out.println("digues el nom del alumne: " + (i + 1));
			noms[i] = reader.nextLine();
		}

		while (k < nota1.length) {
			try {
				System.out.print("Escriu la nota 1 de: " + noms[k]);
				numero2 = reader.nextFloat();
				if (numero2 >= 0 || numero2 <= 10) {
					nota1[k] = numero2;
					k++;
				} else
					System.out.println("La nota ha de ser un numero entre 0 y 10!");

			} catch (Exception e) {
				System.out.println("Ha de ser un numero");
				reader.nextLine();
			}
		}

		k = 0;
		numero2 = 0;

		while (k < nota2.length) {
			try {
				System.out.print("Escriu la nota 2 de: " + noms[k]);
				numero2 = reader.nextFloat();
				if (numero2 >= 0 || numero2 <= 10) {
					nota2[k] = numero2;
					k++;
				} else
					System.out.println("La nota ha de ser un numero entre 0 y 10!");

			} catch (Exception e) {
				System.out.println("Ha de ser un numero");
				reader.nextLine();
			}
		}

		System.out.print("Notes del primer examen (per comrovar): ");
		for (int h = 0; h < nota1.length; h++) {
			System.out.print(nota1[h] + " ");
		}

		System.out.println(" ");

		System.out.print("Notes del segon examen (per comrovar): ");
		for (int h = 0; h < nota2.length; h++) {
			System.out.print(nota2[h] + " ");
		}

		System.out.println(" ");

		for (int l = 0; l < notaf.length; l++) {
			notaf[l] = (nota1[l] + nota2[l]) / 2;
		}

		for (int l = 0; l < notaf.length; l++) {
			System.out.println(noms[l] + " : " + notaf[l]);
		}

		reader.close();
	}

}
