import java.util.Scanner;

public class QualificacionsBlanca {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int alumnes = 0;
		boolean correcte = false;
		String[] nomalumnes = new String[alumnes];
		double[] nota1 = new double[alumnes];
		double[] nota2 = new double[alumnes];
		double nota = 0;

		System.out.println("De quants alumnes has de registrar les notes?");
		do {
			correcte = reader.hasNextInt();
			if (correcte) {
				alumnes = reader.nextInt();
			}
			if (!correcte || alumnes <= 0) {
				System.out.println("Aix� no �s un n�mero v�lid.");
				reader.nextLine();
				correcte = false;
			}
		} while (!correcte);
		for (int i = 0; i < alumnes; i++) {
			System.out.println("Digues el nom del " + (i + 1) + " alumne:");
			nomalumnes[i] = reader.nextLine();
		}
		for (int i = 0; i < alumnes; i++) {
			System.out.println("Quina nota ha tret " + nomalumnes[i] + " al primer examen?");
			do {
				correcte = reader.hasNextDouble();
				if (correcte) {
					nota1[i] = reader.nextDouble();
				}
				if (!correcte || nota1[i] < 0 || nota1[i] > 10) {
					System.out.println("Aix� no �s un n�mero entre 0 i 10. Introdueix una nota v�lida: ");
					reader.nextLine();
					correcte = false;
				}
			} while (!correcte);
			System.out.println("Quina nota ha tret " + nomalumnes[i] + " al segon examen?");
			do {
				correcte = reader.hasNextDouble();
				if (correcte) {
					nota2[i] = reader.nextDouble();
				}
				if (!correcte || nota2[i] < 0 || nota2[i] > 10) {
					System.out.println("Aix� no �s un n�mero entre 0 i 10. Introdueix una nota v�lida: ");
					reader.nextLine();
					correcte = false;
				}
				reader.nextLine();
			} while (!correcte);
		}
		for (int i = 0; i < alumnes; i++) {
			System.out.print(nomalumnes[i] + ": ");
			if (nota1[i] > nota2[i]) {
				nota = nota1[i] * 0.3 + nota2[i] * 0.7;
			} else {
				nota = nota1[i] * 0.1 + nota2[i] * 0.9;
			}
			if ((nota1[i] < 2 || nota2[i] < 2) && nota > 4) {
				nota = 4;
			}
			System.out.print(nota + "\n");
		}
		reader.close();
	}
}