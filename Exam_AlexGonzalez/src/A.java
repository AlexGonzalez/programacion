import java.util.Scanner;

public class A {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int trebessen = 0, majors = 0;
		float total = 0;
		String[][] vacuna = new String[1][5];
		int[][] vacunes = new int[3][4];

		vacuna[0][0] = "Comunitat -";
		vacuna[0][1] = "TrebEss -";
		vacuna[0][2] = "< 65 -";
		vacuna[0][3] = "+ Vac -";
		vacuna[0][4] = "% Vac";

		System.out.println("A Catalunya -->");
		System.out.print("Quantes persones hi ha vacunades del grup de treballadors essencials? :");
		trebessen = reader.nextInt();
		vacunes[0][0] = trebessen;
		System.out.print("I de majors de 65 anys? :");
		majors = reader.nextInt();
		vacunes[0][1] = majors;
		total = majors + trebessen;
		vacunes[0][2] = (int) total;
		total = total / 7700000 * 100;
		vacunes[0][3] = (int) total;

		System.out.println("A Madrid -->");
		System.out.print("Quantes persones hi ha vacunades del grup de treballadors essencials? :");
		trebessen = reader.nextInt();
		vacunes[1][0] = trebessen;
		System.out.print("I de majors de 65 anys? :");
		majors = reader.nextInt();
		vacunes[1][1] = majors;
		total = majors + trebessen;
		vacunes[1][2] = (int) total;
		total = total / 3300000 * 100;
		vacunes[1][3] = (int) total;

		System.out.println("A Castilla i Lleo -->");
		System.out.print("Quantes persones hi ha vacunades del grup de treballadors essencials? :");
		trebessen = reader.nextInt();
		vacunes[2][0] = trebessen;
		System.out.print("I de majors de 65 anys? :");
		majors = reader.nextInt();
		vacunes[2][1] = majors;
		total = majors + trebessen;
		vacunes[2][2] = (int) total;
		total = total / 2400000 * 100;
		vacunes[2][3] = (int) total;

		System.out.println(
				"Treballadors essencials (TrebEss), Majors 65 anys (< 65), Total vacunats (+Vac), Percentatge Vacunats (% Vac)  ");

		for (int i = 0; i < vacuna.length; i++) {
			for (int j = 0; j < vacuna[i].length; j++) {
				System.out.print(vacuna[i][j] + " ");
			}
			System.out.println();
		}
		for (int i = 0; i < vacunes.length; i++) {

			if (i == 0) {
				System.out.print("Catalunya -");
			} else if (i == 1) {
				System.out.print("Madrid -");
			} else {
				System.out.print("Cast. lleo -");
			}
			for (int j = 0; j < vacunes[i].length; j++) {
				if (j == 3) {
					System.out.print(" " + vacunes[i][j] + "  ");
				} else {
					System.out.print(" " + vacunes[i][j] + " - ");
				}
			}
			System.out.println();
		}

		reader.close();
	}
}
