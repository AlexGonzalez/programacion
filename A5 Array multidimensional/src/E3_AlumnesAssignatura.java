import java.util.Scanner;
public class E3_AlumnesAssignatura {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int[][] taula=new int[6][8];
		char classe = 'A';
		
		for(int i =0; i< taula.length;i++) {
			System.out.println("Clase " + classe + " : ");
			for(int j =0; j< taula[i].length;j++) {
				System.out.print("Digues el nombre d'alumnes que hi ha en ");
				if(j==0)System.out.println("mates: ");
				if(j==1)System.out.println("física: ");
				if(j==2)System.out.println("química: ");
				if(j==3)System.out.println("biología: ");
				if(j==4)System.out.println("Catalá: ");
				if(j==5)System.out.println("Castellá: ");
				taula[i][j]=reader.nextInt();
			}
			classe++;
		}
		
		classe='A';
		System.out.println(" ");
		System.out.println("Nombre d'alumnes per grup: ");
		for(int i =0; i< taula.length;i++) {
			System.out.println("Clase " + classe + " : ");
			for(int j =0; j< taula[i].length;j++) {
				System.out.print("Alumnes que hi ha en ");
				if(j==0)System.out.print("mates: ");
				if(j==1)System.out.print("física: ");
				if(j==2)System.out.print("química: ");
				if(j==3)System.out.print("biología: ");
				if(j==4)System.out.println("Catalá: ");
				if(j==5)System.out.println("Castellá: ");
				System.out.println(taula[i][j]+ " ");
			}
			classe++;
		}
		
		reader.close();
		}

}
