/**
 * @author Richard Heredia Roca

nota del autor: En este ejercicio he notado un cambio a considerar. A la hora de multiplicar las matrices, he podido diferenciar un cambio a la hora de ser simetricas o no. 
Por esa razón, he decidido separarlas, ya que las dos en un mismo código no son compatibles.

 lo único que cambia es un numero (Compare linea 131/149), que aun sigo sin saber del todo por que afecta tanto al resultado de este ejercicio
de todas formas, el ejercicio funciona con los criterios establecidos anteriormente
Gracias por leerme :)
 */
import java.util.Scanner;
public class Ex12_NouMultMatriu {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int[][] ma;
		int[][] mb;
		int[][] mr;
		int fila = 0;
		int columna = 0;
		int suma = 0;
		int canvi =0;
		int canvi2=0;
		
		boolean simetriques = false;

		boolean correcte = false;

		do {
			while (!correcte) {
				try {

					do {
						System.out.println("Digues el numero de files de la matriu A: ");
						fila = reader.nextInt();
						System.out.println("Digues el numero de columnes de la matriu A: ");
						columna = reader.nextInt();
						if (fila <= 0 || columna <= 0)
							System.out.println("Han de ser numeros positius!");
						else
							correcte = true;
					} while (fila <= 0 && columna <= 0);

				} catch (Exception e) {
					System.out.println("Ha de ser un numero positiu!");
					reader.nextLine();
				}
			}

			ma = new int[fila][columna];
			correcte = false;

			while (!correcte) {
				try {

					do {
						System.out.println("Digues el numero de files de la matriu B: ");
						fila = reader.nextInt();
						System.out.println("Digues el numero de columnes de la matriu B: ");
						columna = reader.nextInt();
						if (fila <= 0 || columna <= 0)
							System.out.println("Han de ser numeros positius!");
						else
							correcte = true;
					} while (fila <= 0 && columna <= 0);

				} catch (Exception e) {
					System.out.println("Ha de ser un numero positiu!");
					reader.nextLine();
				}
			}

			mb = new int[fila][columna];
			correcte = false;

			if (ma[0].length != mb.length)
				System.out.println("El numero de columnes d'A ha de ser igual al numero de files de B");
		} while (ma[0].length != mb.length);

		//vemos si son simetricas o no
		if(ma.length==mb.length&&ma[0].length==mb[0].length)simetriques = true;
		System.out.println("Simetriques: "+ simetriques);
		for (int i = 0; i < ma.length; i++) {
			for (int j = 0; j < ma[i].length; j++) {
				System.out.println("Digues el numero de la posició (" + (i + 1) + "," + (j + 1) + ") de la matriu A:");
				ma[i][j] = reader.nextInt();
			}
		}

		for (int i = 0; i < mb.length; i++) {
			for (int j = 0; j < mb[i].length; j++) {
				System.out.println("Digues el numero de la posició (" + (i + 1) + "," + (j + 1) + ") de la matriu B:");
				mb[i][j] = reader.nextInt();
			}
		}

		System.out.println(" ");
		for (int i = 0; i < ma.length; i++) {
			for (int j = 0; j < ma[i].length; j++) {
				System.out.print(ma[i][j] + " ");
			}
			System.out.println(" ");
		}
		
		System.out.println(" ");
		for (int i = 0; i < mb.length; i++) {
			for (int j = 0; j < mb[i].length; j++) {
				System.out.print(mb[i][j] + " ");
			}
			System.out.println(" ");
		}
		
		

		// la matriu resposta será sempre el numero de files de la primera matriu i el
		// numero de columnes de la segona matriu.
		mr = new int[ma.length][mb[0].length];
		System.out.println(" ");
		
		if(simetriques == true) {
		while(canvi < ma.length) {
			//el canvi cambia a la fila de la matriz a i canvi 2 cambia la columna. De esa forma, con un bucle, podemos hacer la fila por su columna
			for(int i =0; i < ma[0].length;i++) {
				suma = suma + ma[canvi][i] * mb[i][canvi2];
			}
			mr[canvi][canvi2]= suma;
			suma =0;
			
			//si vemos que canvi 2 se pasa de la columna, volvemos a empezar en una nueva fila de la matriz a. De ahí que sumemos el canvi
			if(canvi2 == mb.length-1) {
				canvi2=0;
				canvi++;
			}
			//en caso contrario, seguimos en la misma fila de A mientras pasamos a la siguiente columna de b
			else canvi2++;
		}
		}
		else {
			if(ma[0].length<3)
			while(canvi < ma.length) {
				//el canvi cambia a la fila de la matriz a i canvi 2 cambia la columna. De esa forma, con un bucle, podemos hacer la fila por su columna
				for(int i =0; i < ma[0].length;i++) {
					suma = suma + ma[canvi][i] * mb[i][canvi2];
				}
				mr[canvi][canvi2]= suma;
				suma =0;
				
				//si vemos que canvi 2 se pasa de la columna, volvemos a empezar en una nueva fila de la matriz a. De ahí que sumemos el canvi
				if(canvi2 == mb.length) {
					canvi2=0;
					canvi++;
				}
				//en caso contrario, seguimos en la misma fila de A mientras pasamos a la siguiente columna de b
				else canvi2++;
			}
			else {
				while(canvi < ma.length) {
					//el canvi cambia a la fila de la matriz a i canvi 2 cambia la columna. De esa forma, con un bucle, podemos hacer la fila por su columna
					for(int i =0; i < ma[0].length;i++) {
						if(canvi2 != mb.length-1)suma = suma + ma[canvi][i] * mb[i][canvi2];
					}
					if(canvi2 != mb.length-1) mr[canvi][canvi2]= suma;
					System.out.println(suma);
					suma =0;
					
					//si vemos que canvi 2 se pasa de la columna, volvemos a empezar en una nueva fila de la matriz a. De ahí que sumemos el canvi
					if(canvi2 == mb.length-1) {
						canvi2=0;
						canvi++;
					}
					//en caso contrario, seguimos en la misma fila de A mientras pasamos a la siguiente columna de b
					else canvi2++;
				}
			}
		}
		System.out.println(" ");
		for (int i = 0; i < mr.length; i++) {
			for (int j = 0; j < mr[i].length; j++) {
				System.out.print(mr[i][j] + " ");
			}
			System.out.println(" ");
		}
		
		reader.close();
	}
}
