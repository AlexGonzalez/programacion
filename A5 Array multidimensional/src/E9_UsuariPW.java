import java.util.Scanner;

public class E9_UsuariPW {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		String[][] general = { { "javi", "maria", "patat" }, { "dontworry", "beHappy", "concat" } };
		String un = null;
		String pw = null;
		int indexpw = 0;
		int indexun = 0;
		int intents=0;

		boolean trobat = false;
		boolean pwcorrecte=false;
		int i = 0;

		while(intents < 2 && !pwcorrecte){
		System.out.println("Introdueixi el seu nom d'usuari: ");
		un = reader.nextLine();
		System.out.println("Contrasenya: ");
		pw = reader.nextLine();
		
		System.out.println(general.length);
		
		while (!trobat && i <= general.length) {
			trobat = general[0][i].equals(un);
			if (trobat == true) {
				System.out.println("Usuari Trobat");
				//fem la validació del password
				pwcorrecte=general[1][i].equals(pw);
			}
			else
				i++;
		}
		
		if(pwcorrecte==false) {
			System.out.println("Algun camp dels que ha possat es incorrecte. Premi intro per continuar: ");
			intents++;
			trobat = false;
			i=0;
		}
		else System.out.println("Acces permés!");
		}
		
		if(intents==2)System.out.println("Intents maxims permesos superats!");
		reader.close();
	}
}
