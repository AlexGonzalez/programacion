/**
 * @author Richard Heredia Roca
 */
import java.util.Scanner;
public class Ex11_Identitat {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int[][] id = new int[3][3];
		boolean correcte1=false;
		boolean correcte2=false;
		boolean correcte3=false;
		
		for(int i =0; i< id.length;i++) {
			for(int j =0; j < id[i].length;j++) {
				System.out.println("Digues el numero de la posició ("+ (i+1) +"," + (j+1) + ") :");
				id[i][j]=reader.nextInt();
			}
		}
		
		if(id[0][0]==1 && id[0][1]==0 && id[0][2]==0)correcte1=true;
		if(id[1][0]==0 && id[1][1]==1 && id[1][2]==0)correcte2=true;
		if(id[2][0]==0 && id[2][1]==0 && id[2][2]==1)correcte3=true;

		if(correcte1==true&&correcte2==true&&correcte3==true)System.out.println("La matriu es Identitat");
		else System.out.println("La matriu no es identitat");
		
		
		
		
		reader.close();
	}

}
