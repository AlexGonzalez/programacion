import java.util.Scanner;
public class E1_MultiplicatPer5 {

	public static void main(String[] args) {
		int m[][] = new int [4][3];
		//int num  =0;

		for(int i =0; i < m.length; i++) {
			for (int j =0; j< m[i].length; j++) {
				m[i][j] = (int)(Math.random()*5);
			}
		}
		
		System.out.println("Matriu Inicial: ");
		
		for(int i =0; i < m.length; i++) {
			for (int j =0; j< m[i].length; j++) {
				System.out.print((m[i][j]) + " ");
			}
			System.out.println(" ");
		}
		
		System.out.println(" ");

		for(int i =0; i < m.length; i++) {
			for (int j =0; j< m[i].length; j++) {
				System.out.print((m[i][j]*5) + " ");
			}
			System.out.println(" ");
		}
		
	}

}
