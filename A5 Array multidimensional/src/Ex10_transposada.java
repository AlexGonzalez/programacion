/**
 * @author Richard Heredia Roca
 */

import java.util.Scanner;
public class Ex10_transposada {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int[][] m=new int[3][3];
		int[][] t=new int[3][3];
		int[][] s=new int[3][3];
		
		for(int i =0; i< m.length;i++) {
			for(int j =0; j < m[i].length;j++) {
				System.out.println("Digues el numero de la posició ("+ (i+1) +"," + (j+1) + ") :");
				m[i][j]=reader.nextInt();
			}
		}
		
		for(int i =0; i< m.length;i++) {
			for(int j =0; j < m[i].length;j++) {
				System.out.print(m[i][j]+ " ");
			}
			System.out.println(" ");
		}
		
		System.out.println(" ");
		
		
		
		for(int i =0; i< m.length;i++) {
			for(int j =0; j < m[i].length;j++) {
				t[j][i]=m[i][j];
			}
		}
		
		for(int i =0; i< t.length;i++) {
			for(int j =0; j < t[i].length;j++) {
				System.out.print(t[i][j] + " ");
			}
			System.out.println(" ");
		}
		
		System.out.println(" ");
		System.out.println("La suma de les dos matrius es: ");
		
		for(int i =0; i< m.length;i++) {
			for(int j =0; j < m[i].length;j++) {
				s[i][j]=m[i][j]+t[i][j];
				System.out.print(s[i][j] + " ");
			}
			System.out.println(" ");
		}
		
		
		reader.close();
	}

}
