import java.util.Scanner;
public class E4_PetitGran {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int major =0;
		int menor =0;
		int x=0;
		int y=0;
		int mx=0;
		int my=0;
		int[][] A=new int[5][10];
	
		for(int i =0; i < A.length; i++) {
			for (int j =0; j< A[i].length; j++) {
				A[i][j] = (int)(Math.random()*11);
			}
		}
		
System.out.println("Matriu Inicial: ");
		
		for(int i =0; i < A.length; i++) {
			for (int j =0; j< A[i].length; j++) {
				System.out.print((A[i][j]) + " ");
			}
			System.out.println(" ");
		}
		
		System.out.println(" ");
		menor = A[0][0];
		major = A[0][0];
		for(int i =0; i < A.length; i++) {
			for (int j =0; j< A[i].length; j++) {
				if(A[i][j] > major) {
					major=A[i][j];
					x=i + 1;
					y=j + 1;
				}
				if(A[i][j] < menor) {
					menor=A[i][j];
					mx=i + 1;
					my=j + 1;
				}
			}
		}
		System.out.println(" ");
		
		System.out.println("Major: " + major + " en la posició (" + x + " " + y + ")");
		System.out.println("Menor: "+ menor+ " en la posició (" + mx + " " + my + ")");
		reader.close();
	}

}
