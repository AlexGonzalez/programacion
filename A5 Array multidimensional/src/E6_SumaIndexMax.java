import java.util.Scanner;
public class E6_SumaIndexMax {

	public static void main(String[] args) {
		int numfc=0;
		int aux=0;
		int num=0;
		int[][]matriu;
		boolean correcte =false;
		
		Scanner reader = new Scanner(System.in);
		
		while (numfc <= 0 && correcte ==false) {
			try {
				System.out.println("Digues el numero de files i de columnes que tindrà la matriu: ");
				numfc = reader.nextInt();
				if (numfc <= 0) {
					System.out.println("El número ha de ser superior a 0");
				}
				else correcte = true;
			} catch (Exception e) {
				System.out.println("ha de ser un número enter");
				reader.nextLine();
			}
		}
		correcte = false;

		
		matriu=new int[numfc][numfc];
		num=(matriu.length-1) * -1;
		aux=num;
		for (int i = 0; i < matriu.length; i++) {
			num=aux+i;
			for (int j = 0; j < matriu[i].length; j++) {
				matriu[i][j]=num;
				System.out.print( matriu[i][j] + " ");
				num++;
			}
			System.out.println(" ");
		}

		reader.close();
	}

}
