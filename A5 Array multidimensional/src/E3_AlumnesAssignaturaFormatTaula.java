import java.util.Scanner;

public class E3_AlumnesAssignaturaFormatTaula {

	public static void main(String[] args) {

		//cosas a mejorar en la siguiente version: 
		//1. Podriamos hacer dos arrais de strings que guarden las materias y los grupos y luego una matriz de ints que guarden las notas. Así comrpovaríamos si
		//los datos estan bien hechos
		String[][] t = new String[6][8];
		Scanner reader = new Scanner(System.in);

		// inicialització de la fila i columna de grups y materies

		t[0][0] = "      ";
		// clases
		t[0][1] = "Prog";
		t[0][2] = "BBDD";
		t[0][3] = "S.O ";
		t[0][4] = "FOL ";
		t[0][5] = "LL.M";
		t[0][6] = "Prac";
		t[0][7] = "Empr";

		// grups
		t[1][0] = "Grup A";
		t[2][0] = "Grup B";
		t[3][0] = "Grup C";
		t[4][0] = "Grup D";
		t[5][0] = "Grup E";

		//comencem per la fila y columna 1, ja que la columna 0 ja ha sigut inicialitzada al principi
		for (int x = 1; x < t.length; x++) {
			for (int y = 1; y < t[x].length; y++) {
				System.out.println(
						"Digues els alumnes que hi ha en el " + t[x][0] + " en la materia de " + t[0][y] + " :");
				t[x][y] = reader.nextLine().trim();
			}
		}

		System.out.println(" ");

		for (int x = 0; x < t.length; x++) {
			for (int y = 0; y < t[x].length; y++) {
				if (x > 0 && y > 0 && y != 0) {
					if (t[x][y].length() > 1)
						System.out.print("| " + t[x][y] + " |");
					else
						System.out.print("|  " + t[x][y] + " |");
				} else
					System.out.print("|" + t[x][y] + "|");

			}
			System.out.println(" ");
		}
		reader.close();
	}
}
