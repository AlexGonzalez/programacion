import java.util.Scanner;

public class E5_SalaDeCine {

	public static void main(String[] args) {

		int buit = 0;
		int reservat = 1;
		int opcio = 0;
		int disponibles = 0;

		int fila = -1;
		int seient = -1;
		int[][] sala = new int[5][5];
		
		boolean correcte = false;
		boolean continua = false;
		Scanner reader = new Scanner(System.in);
		do {
			System.out.println("\t\t -Sala de cine Institut Sabadell-");
			System.out.println("\t1.-Buida la sala");
			System.out.println("\t2.-Visualitzar seients disponibles");
			System.out.println("\t3.-Reserva seients");
			System.out.println("\t4.-Sortir");
			System.out.println("\t----------------------------------------------");
			System.out.println("");

			System.out.println("Tria una opció: ");
			opcio = reader.nextInt();
			switch (opcio) {
			////////////////////////////////////////////////
			case 1:
				for (int i = 0; i < sala.length; i++) {
					for (int j = 0; j < sala[i].length; j++) {
						sala[i][j] = buit;
					}
				}
				System.out.println("Sala buidada correctament!");
				break;
			//////////////////////////////////////////////
			case 2:
				for (int i = 0; i < sala.length; i++) {
					for (int j = 0; j < sala[i].length; j++) {
						if(sala[i][j] == buit) {
							System.out.print(" _ ");
							disponibles++;
						}
						if( sala[i][j] == reservat)System.out.print(" * ");
					}
					System.out.println(" ");
				}
				System.out.println("Hi han disponibles: " + disponibles + " seients!");
				disponibles = 0;
				break;
			//////////////////////////////////////////////
			case 3:
				while(!continua) {
				while (fila < 0 && correcte ==false) {
					try {
						System.out.println("Digues la fila on vols reservar: ");
						fila = reader.nextInt();
						fila=fila-1;
						if (fila < 0 || fila >= sala.length) {
							System.out.println("El numero ha de ser superior a 0");
							fila = -1;
						}
						else correcte = true;
					} catch (Exception e) {
						System.out.println("ha de ser un numero enter");
						reader.nextLine();
					}
				}
				correcte = false;
				while (seient < 0&& correcte == false) {
					try {
						System.out.println("Digues el seient on vols reservar: ");
						seient = reader.nextInt();
						seient = seient -1;
						if (seient < 0 || seient >= sala[fila].length) {
							System.out.println("numero incorrecte");
							seient = -1;
						}
						else correcte = true;
					} catch (Exception e) {
						System.out.println("ha de ser un numero enter");
						reader.nextLine();
					}
				}
				correcte = false;
				if(sala[fila][seient]==reservat) {
					System.out.println("Aquest seient ja está reservat! Sis plau, reservi un altre: ");
					fila = -1;
					seient = -1;
				}
				else continua = true;
				}
				
				sala[fila][seient] = reservat;
				fila = -1;
				seient = -1;
				continua = false;
				System.out.println("Reserva feta satisfactoriament!");
				break;
			///////////////////////////////////////////////////////////
			default:
				System.out.println("Fins aviat!");
				break;
			}
		} while (opcio != 4);

		reader.close();
	}

}
