import java.util.Scanner;

public class Ex12_NouNouMultMatriu {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = 0;
		int[][] m1;
		int[][] m2;
		int[][] mr;

		int contador1 = 0;// me servir� para indicar cuantas veces he multiplicado la misma fila
		int filaacutal = 0;
		int columnaactual = 0;

		System.out.println("Digues el numero de files de la matriu 1: ");
		x1 = reader.nextInt();
		System.out.println("Digues el numero de columnes de la matriu 1: ");
		y1 = reader.nextInt();
		System.out.println("Digues el numero de files de la matriu 2: ");
		x2 = reader.nextInt();
		System.out.println("Digues el numero de files de la matriu 2: ");
		y2 = reader.nextInt();
		m1 = new int[x1][y1];
		m2 = new int[x2][y2];

		if (x1 != y2)
			System.out.println(
					"El numero de columnes de la matriu 1 ha de ser el mateix que el numero de files de la matriu 2");
		else {
			for (int i = 0; i < m1.length; i++) {
				for (int j = 0; j < m1[i].length; j++) {
					System.out.println(
							"Digues el numero de la posició (" + (i + 1) + "," + (j + 1) + ") de la matriu A:");
					m1[i][j] = (int) (Math.random() * 9);
				}
			}

			for (int i = 0; i < m2.length; i++) {
				for (int j = 0; j < m2[i].length; j++) {
					System.out.println(
							"Digues el numero de la posició (" + (i + 1) + "," + (j + 1) + ") de la matriu A:");
					m2[i][j] = (int) (Math.random() * 9);
				}
			}

			mr = new int[m1.length][m2[0].length];
			System.out.println(" ");
			for (int i = 0; i < m1.length; i++) {
				for (int j = 0; j < m1[i].length; j++) {
					System.out.print(m1[i][j] + " ");
				}
				System.out.println(" ");
			}

			System.out.println(" ");
			for (int i = 0; i < m2.length; i++) {
				for (int j = 0; j < m2[i].length; j++) {
					System.out.print(m2[i][j] + " ");
				}
				System.out.println(" ");
			}

			for (int i = 0; i < m1.length; i++) {
				for (int j = 0; j < m2[0].length; j++) {
					for (int k = 0; k < m2.length; k++) {
						mr[i][j] = mr[i][j] + m1[i][k] * m2[k][j];
					}
				}
			}

			System.out.println(" ");

			for (int i = 0; i < mr.length; i++) {
				for (int j = 0; j < mr[i].length; j++) {
					System.out.print(mr[i][j] + " ");
				}
				System.out.println(" ");
			}

		}

		reader.close();
	}

}
