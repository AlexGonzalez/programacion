import java.util.Scanner;
public class E7_EstaEnLaMatriu {

	public static void main(String[] args) {
		int num=0;
		int fila=0;
		int columna =0;
		boolean trobat=false;
		String[][] noms = new String[2][2];
		String buscat=null;
		Scanner reader = new Scanner(System.in);
		

		for(int i =0; i<noms.length; i++) {
			for(int j =0; j<noms[i].length;j++) {
				System.out.print("Digues el nom " + (j+1) + " de la fila " +(i+1)+ " : ");
				noms[i][j]=reader.nextLine().toLowerCase().trim();
			}
		}
		
		System.out.println("Digues el nom a buscar: ");
		buscat = reader.nextLine().toLowerCase().trim();
		
		while(!trobat&&fila<noms.length) {
			while(!trobat&&columna < noms[fila].length) {
				trobat=buscat.equals(noms[fila][columna]);
				if(trobat == true) {
					System.out.println("El nom es troba en la llista");
				}
				else columna++;
			}
			columna =0;
			fila++;
		}
		
		if(trobat==false) System.out.println("El nom no esta en la llista.");
		reader.close();
	}

}
