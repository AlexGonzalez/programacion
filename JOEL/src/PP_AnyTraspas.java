

import java.util.Scanner;

public class PP_AnyTraspas {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int any = 0;
		any = reader.nextInt();

		if (((any % 4 == 0) && !(any % 100 == 0)) || (any % 400 == 0)) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}

		reader.close();
	}

}
