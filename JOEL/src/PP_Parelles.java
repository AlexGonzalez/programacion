
import java.util.Scanner;

public class PP_Parelles {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int c1 = 0, c2 = 0, c3 = 0;
		c1 = reader.nextInt();
		c2 = reader.nextInt();
		c3 = reader.nextInt();
		if (c1 == c2 || c2 == c3 || c1 == c3) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}

		reader.close();
	}

}
