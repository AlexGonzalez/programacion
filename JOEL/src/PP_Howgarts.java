

import java.util.Scanner;

public class PP_Howgarts {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		String howgarts;
		howgarts = reader.next();

		switch (howgarts.toLowerCase()) {
		case "coratge":
			System.out.println("Gryffindor");
			break;
		case "coneixement":
			System.out.println("Ravenclaw");
			break;
		case "ambicio":
			System.out.println("Slytherin");
			break;
		default:
			System.out.println("Hufflepuff");
			break;
		}

		reader.close();
	}
}
