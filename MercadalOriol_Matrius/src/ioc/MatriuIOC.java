package ioc;

import java.util.Scanner;

//Un programa que inicialitza un array bidimensional.
public class MatriuIOC {
	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
//Llegeix les files.
		int nombreFiles = 0;
		while (nombreFiles <= 0) {
			System.out.print("Quantes files tindr� la taula? ");
			if (lector.hasNextInt()) {
				nombreFiles = lector.nextInt();
			} else {
				lector.next();
				System.out.print("Aquest valor no �s correcte. ");
			}
		}
		lector.nextLine();
//Llegeix les columnes.
		int nombreColumnes = 0;
		while (nombreColumnes <= 0) {
			System.out.print("Quantes files tindr� la taula? ");
			if (lector.hasNextInt()) {
				nombreColumnes = lector.nextInt();
			} else {
				lector.next();
				System.out.print("Aquest valor no �s correcte. ");
			}
		}
		lector.nextLine();
//Inicialitzaci� amb valors per defecte.
		int[][] arrayBidi = new int[nombreFiles][nombreColumnes];
//Observeu l'�s de l'atribut "length".
		System.out.println("Hi ha " + arrayBidi.length + " files.");
		System.out.println("Hi ha " + arrayBidi[0].length + "columnes.");
//Bucle per rec�rrer cada fila.
//"i" indica el n�mero de fila.
		for (int i = 0; i < nombreFiles; i++) {
//Bucle per rec�rrer cada posici� dins de la fila (columnes de la fila).
//"j" indica el n�mero de fila.
			for (int j = 0; j < nombreColumnes; j++) {
//Valor assignat a la posici�: suma dels �ndex de fila i columna.
				arrayBidi[i][j] = i + j;
			}
		}
//Es visualitza el resultat, tamb� calen dos bucles.
		for (int i = 0; i < nombreFiles; i++) {
//Inici de fila, obrim claud�tors.
			System.out.print("Fila " + i + " { ");
			for (int j = 0; j < nombreColumnes; j++) {
				System.out.print(arrayBidi[i][j] + " ");
			}
//Al final de cada fila es tanquen claud�tors i es fa un salt de l�nia.
			System.out.println("}");
		}
		lector.close();
	}
}