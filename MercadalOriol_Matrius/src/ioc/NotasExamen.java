package ioc;

import java.util.Scanner;

public class NotasExamen {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float[][] arrayBidiNotes =new float[4][4];
		int estudiant = -1;
		int i = 0;
		while ((estudiant == -1) && (i < arrayBidiNotes.length)) {
			int j = 0;
			while ((estudiant == -1) && (j < arrayBidiNotes[i].length)) {
				if (arrayBidiNotes[i][j] == 0f) {
					estudiant = i;
				}
				j++;
			}
			i++;
		}
		if (estudiant == -1) {
			System.out.println("Cap estudiant no t� un 0.");
		} else {
			System.out.println("L'estudiant " + estudiant + " t� un 0.");
		}
		sc.close();
	}
}