package ioc;

import java.util.Scanner;

//Un programa que inicialitza un array bidimensional.
public class MatriuIOC2 {
	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);

		int nombreFiles = 0, valor;
		while (nombreFiles <= 0) {
			System.out.print("Quantes files tindr� la taula? ");
			if (lector.hasNextInt()) {
				nombreFiles = lector.nextInt();
			} else {
				lector.next();
				System.out.print("Aquest valor no �s correcte. ");
			}
		}
		lector.nextLine();
		int nombreColumnes = 0;
		while (nombreColumnes <= 0) {
			System.out.print("Quantes files tindr� la taula? ");
			if (lector.hasNextInt()) {
				nombreColumnes = lector.nextInt();
			} else {
				lector.next();
				System.out.print("Aquest valor no �s correcte. ");
			}
		}
		lector.nextLine();
		int[][] arrayBidi = new int[nombreFiles][nombreColumnes];
		System.out.println("Hi ha " + arrayBidi.length + " files.");
		System.out.println("Hi ha " + arrayBidi[0].length + "columnes.");

		for (int i = 0; i < nombreFiles; i++) {

			for (int j = 0; j < nombreColumnes; j++) {
				System.out.println("Valor de la fila " + (i + 1) + " de la matriz: ");
				do {
					valor = lector.nextInt();
					if (valor == 0) {
						System.out.println("Tiene que ser mayor a 0");
					}
				} while (valor == 0);

				arrayBidi[i][j] = valor;
			}
		}
		for (int i = 0; i < nombreFiles; i++) {

			System.out.print("Fila " + i + " { ");
			for (int j = 0; j < nombreColumnes; j++) {
				System.out.print(arrayBidi[i][j] + " ");
			}
			System.out.println("}");
		}
		lector.close();
	}
}