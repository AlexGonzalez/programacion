package ioc;

public class Notas_examen_0 {
	public static void main(String[] args) {
		float[][] arrayBidiNotes = { { 4.5f, 6f, 5f, 8f }, { 10f, 8f, 7.5f, 9.5f }, { 3f, 2.5f, 0f, 6f },
				{ 6f, 8.5f, 6f, 4f }, { 9f, 7.5f, 7f, 8f } };
		int estudiant = -1;
		int i = 0;
		while ((estudiant == -1) && (i < arrayBidiNotes.length)) {
			int j = 0;
			while ((estudiant == -1) && (j < arrayBidiNotes[i].length)) {
				if (arrayBidiNotes[i][j] == 0f) {
					estudiant = i;
				}
				j++;
			}
			i++;
			System.out.println("El alumno"+(i+1)+"ha sacado un 0 en el examen "+(j+1));
		}
		if (estudiant == -1) {
			System.out.println("Cap estudiant no té un 0.");
		} else {
			System.out.println("L'estudiant " + estudiant + " té un 0.");
		}
	}
}