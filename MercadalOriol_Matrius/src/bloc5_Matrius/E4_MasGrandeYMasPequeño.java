package bloc5_Matrius;
/**
 * 
 * @author MercadalOriol
 *
 */
public class E4_MasGrandeYMasPequeño {
	public static void main(String[] args) {
		final int FILA = 5, COL = 10;
		int[][] numeros = new int[FILA][COL];
		int random = 0, maximo = 100, minimo = 0, range = maximo - minimo + 1, filaMax = 0, colMax = 0, filaMin = 0,
				colMin = 0, numMax = 0, numMin = 100;
		for (int i = 0; i < FILA; i++) {
			for (int j = 0; j < COL; j++) {
				random = (int) (Math.random() * range) + minimo;
				numeros[i][j] = random;
				if (numeros[i][j] > numMax) {
					numMax = numeros[i][j];
					filaMax=i;
					colMax=j;
				}
				if (numeros[i][j] < numMin) {
					numMin = numeros[i][j];
					filaMin=i;
					colMin=j;
				}
			}
		}
		for (int i = 0; i < FILA; i++) {
			for (int j = 0; j < COL; j++) {
				System.out.print(numeros[i][j] + " ");

			}
			System.out.println();
		}
		System.out.println(
				"El numero m�ximo es " + numMax + " esta en la fila " + (filaMax + 1) + " columna " + (colMax + 1));
		System.out.println(
				"El numero m�nimo es " + numMin + " esta en la fila " + (filaMin + 1) + " columna " + (colMin + 1));
	}
}
