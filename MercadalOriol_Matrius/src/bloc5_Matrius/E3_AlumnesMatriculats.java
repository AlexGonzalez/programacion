package bloc5_Matrius;

import java.util.Scanner;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E3_AlumnesMatriculats {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final int ROW = 6, COL = 8;
		String[] uf = { "UF1", "UF2", "UF3", "UF4", "UF5", "UF6", "UF7", "UF8" };
		String[] grupo = { "Grupo 1", "Grupo 2", "Grupo 3", "Grupo 4", "Grupo 5", "Grupo 6" };
		int[] total = new int[8];
		int[][] numAsignados = new int[ROW][COL];
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COL; j++) {
				System.out.println("Introduce el numero de alumnos del " + grupo[i] + " de la " + uf[j] + "?");
				numAsignados[i][j] = sc.nextInt();
			}
		}
		for (int i = 0; i < COL; i++) {
			for (int j = 0; j < ROW; j++) {
				total[i] = numAsignados[j][i] + total[i];
			}
		}
		System.out.println();
		System.out.print("          |");
		for (int i = 0; i < uf.length; i++) {
			System.out.print(uf[i] + "|");
		}
		System.out.println();
		for (int i = 0; i < ROW; i++) {
			System.out.print("| " + grupo[i] + " |");
			for (int j = 0; j < COL; j++) {
				System.out.print(" " + numAsignados[i][j] + " |");
			}
			System.out.println();
		}
		System.out.print("|  Total  |");
		for (int i = 0; i < total.length; i++) {
			if (total[i] >= 10) {
				System.out.print(" " + total[i] + "|");
			} else {
				System.out.print(" " + total[i] + " |");
			}
		}
		sc.close();
	}
}
