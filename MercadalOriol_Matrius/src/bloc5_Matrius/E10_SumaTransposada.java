package bloc5_Matrius;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E10_SumaTransposada {

	public static void main(String[] args) {
		int[][] num1 = { { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 } };
		int[][] num2 = { { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 } };
		int[][] res = new int[3][3];
		for (int i = 0; i < res.length; i++) {
			for (int j = 0; j < res[i].length; j++) {
				res[i][j] = num1[i][j] + num2[i][j];
			}
		}
		for (int i = 0; i < res.length; i++) {
			for (int j = 0; j < res[i].length; j++) {
				System.out.println(num1[j][i]+" + " +num2[j][i]+" = "+res[j][i]);
			}
		}
	}

}
