package bloc5_Matrius;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E8_NombresAlfabeticamente {
	public static void main(String[] args) {
//		Scanner sc = new Scanner(System.in);
//		String name;
//		String[][] nombres = new String[1][10];
//		for (int i = 0; i < 1; i++) {
//			for (int j = 0; j < 10; j++) {
//				System.out.println("Introduce el nombre numero " + (j + 1));
//				nombres[i][j] = sc.nextLine();
//			}
//		}
		Scanner sc = new Scanner(System.in);
		String[] nombres = new String[10];
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce el nombre numero " + (i + 1));
			nombres[i] = sc.nextLine().toLowerCase();
		}
		Arrays.sort(nombres);
		System.out.println("El primer nombre ordenado alfabéticamente es: \"" + nombres[0]+"\"");
		sc.close();
	}
}
