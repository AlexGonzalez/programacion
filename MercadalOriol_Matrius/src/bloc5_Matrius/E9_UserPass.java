package bloc5_Matrius;

import java.util.Scanner;
/**
 * 
 * @author MercadalOriol
 *
 */
public class E9_UserPass {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final int ROW = 5, COL = 1;
		int cont = 0, i2 = 0, j2 = 0;
		boolean userOk = false, passOk = false, loginOk = false;
		String usuario = " ", contrasena = " ";
		String[][] user = new String[ROW][COL];
		String[][] pass = new String[ROW][COL];
		System.out.println("Para poder identificarte, tienes que crear un nombre de usuario y contraseņa");
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COL; j++) {
				System.out.println("Usuario " + (i + 1) + " introduce el nombre de usuario");
				user[i][j] = sc.nextLine();
				System.out.println("Usuario " + (i + 1) + " introduce la contraseņa");
				pass[i][j] = sc.nextLine();
			}
		}
		cont = 5;
		while (cont != 0 && !loginOk) {
			System.out.println("Identificate, introduce tu nombre de usuario:");
			usuario = sc.nextLine();
			System.out.println("Introduce tu contraseņa:");
			contrasena = sc.nextLine();
			passOk = false;
			userOk = false;
			i2 = 0;
			j2 = 0;
			while (i2 < ROW && !userOk) {
				j2=0;
				while (j2 < COL && !userOk) {
					if (user[i2][j2].equals(usuario)) {
						userOk = true;
					}
					j2++;
				}
				i2++;
			}

			if (pass[i2-1][j2-1].equals(contrasena)) {
				passOk = true;
			}
			
			if (passOk && userOk) {
				System.out.println("Login correcto. Bienvenido!");
				loginOk = true;
			} else if (passOk) {
				System.out.println("El usuario es incorrecto.");
			} else if (userOk) {
				System.out.print("La contraseņa es incorrecta. ");
			} else {
				System.out.println("La contraseņa y el usuario son incorrectos.|n");
			}
			cont--;
			if (!loginOk) {

				if (cont != 0) {
					System.out.println("Quedan " + cont + " intentos.\n");
				} else {
					System.out.println("Login fallido.\n");
				}
			}

		}

		sc.close();
	}

}
