package bloc5_Matrius;

import java.util.Scanner;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E7_ContadorNombres {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int row = 5, col = 5, cont=0;
		String nombre;
		String[][] nombres = new String[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.println("escribe el nombre de la fila "+(i+1)+" y de la columna "+(j+1) );
				nombres[i][j]=sc.nextLine().toLowerCase();
			}
		}
		System.out.println("Introduce un nombre y te dir� cuantas veces aparece:");
		nombre=sc.nextLine();
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				if (nombre.equals(nombres[i][j])) {
					cont++;
				}
			}
		}
		System.out.println("El nombre "+nombre+" ha sido introducido "+cont+" de veces.");
		sc.close();
	}

}
