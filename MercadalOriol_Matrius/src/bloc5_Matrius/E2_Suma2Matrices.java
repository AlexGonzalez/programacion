package bloc5_Matrius;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E2_Suma2Matrices {
	public static void main(String[] args) {
		 final int ROW = 4, COL = 2;
		int[][] num1 = { { 2, 3},{ 0, 18 },{ -1, 4},{ 1, 2 } };
		int[][] num2 = { { 1, 5},{ -12, 2 },{ 3, 7},{ 12, 128 } };
		int[][] res = new int[ROW][COL];
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COL; j++) {
				res[i][j]=num1[i][j]+num2[i][j];
			}
		}
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COL; j++) {
				System.out.println(num1[i][j]+"+"+num2[i][j]+"="+res[i][j]);
			}
			if (i==1) {
				System.out.println();
			}
		}
	}

}
