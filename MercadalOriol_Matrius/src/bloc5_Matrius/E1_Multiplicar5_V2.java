package bloc5_Matrius;
/**
 * 
 * @author MercadalOriol
 *
 */
public class E1_Multiplicar5_V2 {
	public static void main(String[] args) {
		final int ROW = 4, COL = 3;
		int[][] matriz = {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COL; j++) {
					System.out.print(matriz[i][j]+"x5="+matriz[i][j]*5+" ");
			}
			System.out.println();
		}
	}
}
