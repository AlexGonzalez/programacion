package bloc5_Matrius;

import java.util.Scanner;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E6_DiagonalCero {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int row = 0, col = 0, cont=0, j = 0, aux = 0;
		System.out.println("Introduce el numero de filas de la matriz:");
		row = sc.nextInt();
		System.out.println("Introduce el numero de columnas de la matriz ");
		col = sc.nextInt();
		int[][] matriz = new int[row][col];
		for (int i = row - 1; i >= 0; i--) {
			j = aux;
			while (cont < col) {
				matriz[i][cont] = j;
				cont++;
				j++;
			}
			aux--;
			cont = 0;
		}
//		for (int i = 0; i <row; i++) {
//			cont=col-1;
//			j = aux;
//			while (cont >= 0) {
//				matriz[i][cont] = j;
//				cont--;
//				j--;
//			}
//			aux++;
//			cont = 0;
//		}
		for (int i = 0; i < row; i++) {
			for (int k = 0; k < col; k++) {
				if (matriz[i][k]<0) {
					System.out.print("["+matriz[i][k] + "] ");
				}else {
					System.out.print("[ "+matriz[i][k] + "] ");
				}
				
			}
			System.out.println();
		}
		sc.close();
	}

}
