package bloc5_Matrius;

import java.util.Scanner;
/**
 * 
 * @author MercadalOriol
 *
 */
public class E5_Cine {
	public static void main(String[] args) {
		final int ROW = 20, COL = 15;
//		final int ROW = 10, COL = 5; // Esto es un caso de prueba
		Scanner sc = new Scanner(System.in);
		int op = 0, numFila = 0, numCol = 0, posFila = 0, posCol = 0, medio = COL/2, posLibreMin = medio,
				posLibreMax = medio;
		boolean correcto = false, encontradoMin = false, encontradoMax = false, errorMin = false, errorMax = false,
				trafficLight = false;
		char[][] asientos = new char[ROW][COL];
		do {

			do {
				System.out.println("Menu de opciones:");
				System.out.println("1.Vaciar Sala ");
				System.out.println("2.Visualizar los asientos disponibles");
				System.out.println("3.Reserva de asientos");
				System.out.println("4.Salir");
				op = sc.nextInt();
				if (op == 1) {
					correcto = true;
				}
			} while (op < 0 || op > 4);
			switch (op) {
			case 1:
				for (int i = 0; i < ROW; i++) {
					for (int j = 0; j < COL; j++) {
						asientos[i][j] = '_';
					}
				}
				encontradoMin = false;
				encontradoMax = false;
				errorMin = false;
				errorMax = false;
				break;
			case 2:
				if (correcto) {
					for (int i = 0; i < ROW; i++) {
						for (int j = 0; j < COL; j++) {
							System.out.print("[" + asientos[i][j] + "] ");
						}
						System.out.println();
					}
				} else {
					System.out.println("Para poder utilizar esta opci�n primero has de pasar por la opci�n numero 1\n");
				}

				break;
			case 3:
				if (correcto) {
					trafficLight=false;
					numFila=0;
					numCol=0;
					while (!trafficLight) {
						try {
							while (numFila < 1 || numFila > 20) {
								System.out.println("Introduce la fila 1 al 20");
								numFila = sc.nextInt();
								if (numFila < 1 || numFila > 20) {
									System.out.println("El n�mero introducido tiene que ser entre 1 y "+ROW);
								}

							}
							while (numCol < 1 || numCol > 15) {
								System.out.println("Introduce el numero de asiento 1 al 15");
								numCol = sc.nextInt();
								if (numCol < 1 || numCol > 15) {
									System.out.println("El numero introducido tiene que ser entre 1 y "+COL);
								}
							}
							trafficLight=true;
						} catch (Exception e) {
							System.out.println("Los valores introducidos tienen que ser un numero entero!\n");
							sc.next();
						}
					}
					posFila = numFila - 1;
					posCol = numCol - 1;
					if (asientos[posFila][posCol] == '_') {
						asientos[posFila][posCol] = '*';
						System.out.println("El asiento se ha reservado correctamente\n");
					} else if (asientos[posFila][posCol] == '*') {
						if (asientos[posFila][medio] == '_') {
							System.out.println("El asiento libre mas cercano al medio es " + (medio + 1));
						} else {
							while (!encontradoMin && !errorMin) {
								posLibreMin = posLibreMin - 1;
								if (posLibreMin >= 0) {
									if (asientos[posFila][posLibreMin] == '_') {
										encontradoMin = true;
									}
								} else {
									errorMin = true;
								}

							}
							while (!encontradoMax && !errorMax) {
								posLibreMax = posLibreMax + 1;
								if (posLibreMax < COL) {
									if (asientos[posFila][posLibreMax] == '_') {
										encontradoMax = true;
									}
								} else {
									errorMax = true;
								}

							}
							if (errorMax && errorMin) {
								System.out.println("No quedan asientos disponibles en esta fila");
							} else if (encontradoMax && errorMin) {
								System.out.println("El asiento libre mas proximo al centro es " + (posLibreMax + 1));
							} else if (encontradoMin && errorMax) {
								System.out.println("El asiento libre mas proximo al centro es " + (posLibreMin + 1));
							} else {
								System.out.println("Los asientos libres mas proximos al centro son " + (posLibreMin + 1)
										+ " y " + (posLibreMax + 1));
							}
						}
					}
					encontradoMin = false;
					encontradoMax = false;
					errorMin = false;
					errorMax = false;

				} else {
					System.out.println("Para poder utilizar esta opci�n primero has de pasar por la opci�n numero 1\n");
				}

				break;
			case 4:
				op = 0;
				break;
			}
		} while (op != 0);
		System.out.println("Adios!");
		sc.close();
	}
}
