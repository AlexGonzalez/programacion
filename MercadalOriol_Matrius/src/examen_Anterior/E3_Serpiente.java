package examen_Anterior;

import java.util.Scanner;

public class E3_Serpiente {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int y = 0, x = 0, posRandomY = 0, posRandomX = 0, maximoY = 0, maximoX = 0, minimo = 0, range = 0;
		;
		char op = '0', aux = '0';
		boolean light = false, fin = false, posOk = false;
		while (!light) {
			try {
				System.out.println("Introduce el numero de filas (1 a 100):");
				y = sc.nextInt();

				System.out.println("Introduce el numero de columnas (1 a 100):");
				x = sc.nextInt();
				if ((y > 100 || y < 1) && (x > 100 || x < 1)) {
					System.out.println("Las filas y columnas solo admiten valores de 1 a 100");
				} else if (x > 100 || x < 1) {
					System.out.println("Las columnas solo admiten valores de 1 a 100");
				} else if (y > 100 || y < 1) {
					System.out.println("Las filas solo admiten valores de 1 a 100");
				} else {
					light = true;
				}
			} catch (Exception e) {
				System.out.println("Solo se admiten valores enteros de 1 a 100");
				sc.next();
			}
		}
		char[][] serpiente = new char[y][x];
		maximoY = y - 1;
		range = maximoY - minimo + 1;
		posRandomY = (int) (Math.random() * range) + minimo;
		maximoX = x - 1;
		range = maximoX - minimo + 1;
		posRandomX = (int) (Math.random() * range) + minimo;
		for (int i = 0; i < serpiente.length; i++) {
			for (int j = 0; j < serpiente[0].length; j++) {
				serpiente[i][j] = '-';
			}
		}
		posRandomX = (int) (Math.random() * range) + minimo;

		serpiente[posRandomY][posRandomX] = '*';
		for (int i = 0; i < serpiente.length; i++) {
			for (int j = 0; j < serpiente[0].length; j++) {
				System.out.print("[" + serpiente[i][j] + "] ");
			}
			System.out.println();
		}
		System.out.println("\nIntroduce una opcion");
		System.out.println("a: Una posici�n hacia arriba");
		System.out.println("b: Una posici�n hacia abajo");
		System.out.println("d: Una posici�n hacia la derecha");
		System.out.println("e: Una posici�n hacia la izquierda");
		System.out.println("0: fin");
		while (!fin) {
			op = sc.next().toLowerCase().charAt(0);
			if (op == 'a' || op == 'b' || op == 'd' || op == 'e') {
				aux = op;
			}
			try {
				while (!posOk) {
					if (op == 'a') {
						serpiente[posRandomY][posRandomX] = '-';
						posRandomY = posRandomY - 1;
						serpiente[posRandomY][posRandomX] = '*';
						posOk = true;
					} else if (op == 'b') {
						serpiente[posRandomY][posRandomX] = '-';
						posRandomY = posRandomY + 1;
						serpiente[posRandomY][posRandomX] = '*';
						posOk = true;
					} else if (op == 'd') {
						serpiente[posRandomY][posRandomX] = '-';
						posRandomX = posRandomX + 1;
						serpiente[posRandomY][posRandomX] = '*';
						posOk = true;
					} else if (op == 'e') {
						serpiente[posRandomY][posRandomX] = '-';
						posRandomX = posRandomX - 1;
						serpiente[posRandomY][posRandomX] = '*';
						posOk = true;
					} else if (op == '0') {
						posOk = true;
						fin = true;
					} else {
						aux = op;
					}
					if (op != 0) {
						for (int i = 0; i < serpiente.length; i++) {
							for (int j = 0; j < serpiente[0].length; j++) {
								System.out.print("[" + serpiente[i][j] + "] ");
							}
							System.out.println();
						}
						System.out.println();
					} else {
						System.out.println("Hasta otra!");
					}

				}
				posOk = false;

			} catch (Exception e) {
				System.out.println("\nGame Over, has muerto.");
				fin = true;
			}

		}
		fin = false;
	}

}
