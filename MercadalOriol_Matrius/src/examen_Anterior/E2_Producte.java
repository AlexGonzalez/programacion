package examen_Anterior;

import java.util.Scanner;

/**
 * 
 * @author MercadalOriol
 *
 */
public class E2_Producte {
//No he conseguido sacarlo, no veo la manera de avanzar con doble matriz y avanzar columnas, filas todo intercalado etc...
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int fila = 0, col = 0, fila1 = 0, col1 = 0, random = 0, maximo = 9, minimo = 0, range = maximo - minimo + 1;
		System.out.println("Introduce el numero de filas:");
		fila = sc.nextInt();
		System.out.println("Introduce el numero de columnas:");
		col = sc.nextInt();
		int[][] factor1 = new int[fila][col];
		System.out.println("Introduce el numero de filas de la segunda matriz:");
		fila1 = sc.nextInt();
		System.out.println("Introduce el numero de columnas de la segunda matriz:");
		col1 = sc.nextInt();
		int[][] factor2 = new int[fila1][col1];
		int[][] resultat = new int[fila][col1];
		for (int i = 0; i < fila; i++) {
			for (int j = 0; j < col; j++) {
				random = (int) (Math.random() * range) + minimo;
				factor1[i][j] = random;
				System.out.print(factor1[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		for (int i = 0; i < fila1; i++) {
			for (int j = 0; j < col1; j++) {
				random = (int) (Math.random() * range) + minimo;
				factor2[i][j] = random;
				System.out.print(factor2[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		if (fila != col1) {
			System.out.println(
					"Error, para poder hacer una multipicaci�n de matriz hay las filas tiene que ser del mismo tama�o que las columnas.");
		} else {
			for (int i = 0; i < fila; i++) {
				for (int j = 0; j < col1; j++) {
					for (int j2 = 0; j2 < fila1; j2++) {
						resultat[i][j] = resultat[i][j] + factor1[i][j2] * factor2[j2][j];
					}
					System.out.print(resultat[i][j]+" ");
				}
				System.out.println();
			}
		}
		sc.close();
	}

}
