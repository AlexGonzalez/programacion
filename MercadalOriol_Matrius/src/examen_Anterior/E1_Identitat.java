package examen_Anterior;

import java.util.Scanner;

public class E1_Identitat {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int fila = 0, col = 0;
		System.out.println("Introduce el numero de filas:");
		fila = sc.nextInt();
		System.out.println("Introduce el numero de columnas:");
		col = sc.nextInt();
		int[][] identitat = new int[fila][col];
		if (fila == col) {
			for (int i = 0; i < identitat.length; i++) {
				identitat[i][i] = 1;
			}
			for (int i = 0; i < identitat.length; i++) {
				for (int j = 0; j < identitat[i].length; j++) {

					System.out.print(identitat[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("Aquesta matriu si es d'identitat");
		} else {
			System.out.println("Aquesta matriu no es d'identitat");
		}
		sc.close();
	}

}
