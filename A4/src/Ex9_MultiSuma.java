import java.util.Scanner;

public class Ex9_MultiSuma {
	public static void main(String[] args) {

			Scanner reader = new Scanner(System.in); 
				
			int num1 = 0, num2 = 0; 
			int producte = 0;
			int cont = 0;
			boolean fi = false;
			
			
			while (!fi)
			{
				System.out.print("Introdueix un número: ");
				try {
					num1 = reader.nextInt();  
					if (num1 > 0) fi = true;
					else  System.out.println("Atenció! introdueix un número positiu");
				} catch (Exception e){
					System.out.println("Atenció! únicament es permet insertar números enters. ");
					reader.nextLine();   
				}	
			}		
			fi = false;
			while (!fi)
			{
				System.out.print("Introdueix un número: ");
				try {
					num2 = reader.nextInt();  
					if (num2 > 0) fi = true;
					else  System.out.println("Atenció! introdueix un número positiu");
				} catch (Exception e){
					System.out.println("Atenció! únicament es permet insertar números enters. ");
					reader.nextLine();    
				}	
			}			
			cont = num1;
			while (cont > 0) {
					producte = producte + num2;
					cont --;
			}							
			System.out.println("El producte " + num1 + " * " + num2 + " = " + producte);
			reader.close();				
		}  
	}
