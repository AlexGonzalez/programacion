import java.util.Scanner;

public class Ex7_NumMesGran {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int num = 0, gran = 0;
		boolean compro = false;
		while (!compro) {
		try {
			System.out.print("Escru un numero (0 per acabar): ");
			num = reader.nextInt();
			
			if (num > gran) {
				gran = num;
			} else if (num ==0) {
				compro = true;
			}
		
			
		}catch (Exception e) {
			System.out.println("Nomes accepta numeros enters:");
			reader.nextLine();
		}
		}
		System.out.println("El numero mes gran es: " + gran);
		
		
	reader.close();	
	}
}
