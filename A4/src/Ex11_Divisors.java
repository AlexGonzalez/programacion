import java.util.Scanner; 

public class Ex11_Divisors {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in); 

		int num = 0;
		int comp = 0;
		boolean correcte = false;
		while (!correcte) {
			System.out.print("Introdueix el valor de n: ");
			try {
				num = reader.nextInt();
				correcte = true;
				if (num == 0) {
					System.out.println("El número " + num + " té infinits divisors");
				} else if (num < 0) {
					for (int divisor = num; divisor < 0; divisor++) {
						if (num % divisor == 0) {
							System.out.println("El número " + divisor + " és divisor de " + num);
							comp++;
						}
					}
					num = Math.abs(num);
					for (int divisor = 1; divisor <= num; divisor++) {
						if (num % divisor == 0) {
							System.out.println("El número " + divisor + " és divisor de " + num);
							comp++;
						}
					}
				} else {
					for (int divisor = 1; divisor <= num; divisor++) {
						if (num % divisor == 0) {
							System.out.println("El número " + divisor + " és divisor de " + num);
							comp++;
						}
					}
				}
				if (num != 0) {
					System.out.println("El número " + num + " té " + comp + " divisors");
				}
			} catch (Exception e) {
				System.out.println("Atenció! Únicament es permeten números enters. ");
				reader.nextLine(); 
			}
		}
		reader.close();
	}
}