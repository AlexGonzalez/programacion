import java.util.Scanner;

public class Ex15_Vocals {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		char lletra;
		int contA = 0;
		int contE = 0;
		int contI = 0;
		int contO = 0;
		int contU = 0;
		int pos = 0;
		System.out.println("Escriu una frase acabada en punt ");
		String input = reader.nextLine();
		lletra = input.charAt(0);
		while (lletra != '.') {
			switch (lletra) {
			case 'a':
			case 'A': {
				contA++;
				break;
			}
			case 'e':
			case 'E': {
				contE++;
				break;
			}
			case 'i':
			case 'I': {
				contI++;
				break;
			}
			case 'o':
			case 'O': {
				contO++;
				break;
			}
			case 'u':
			case 'U': {
				contU++;
				break;
			}
			}
			pos++;
			lletra = input.charAt(pos);
		}
		System.out.println("L'A apareix " + contA + " vegades");
		System.out.println("L'E apareix " + contE + " vegades");
		System.out.println("L'I apareix " + contI + " vegades");
		System.out.println("L'O apareix " + contO + " vegades");
		System.out.println("L'U apareix " + contU + " vegades");
		reader.close();
	}
}