import java.util.Scanner; 

public class Ex16_SequenciaLletra {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in); 
		
		char lletra= ' ';
		char lletraseguent= ' ';
		int compLa=0;
		int pos = 0;
		System.out.println("Escriu una frase acabada en punt ");
		String input = reader.nextLine();  
		lletra = input.charAt(0); 	
		lletraseguent = input.charAt(1);
		while (lletra != '.' && lletraseguent != '.')
		{
			if ((lletra == 'l' || lletra == 'L') && (lletraseguent == 'a' || lletraseguent == 'A'))
			{
				compLa++;
			}
			pos++;
			lletra = input.charAt(pos);
			lletraseguent = input.charAt(pos+1);
		}
		System.out.println("Heu introduit 'la'  " + compLa + " vegades");
		reader.close();
	}  
}

