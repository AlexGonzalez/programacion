import java.util.Scanner;

//1.	Algorisme que llegeix 10 números enters i ens diu quants d’ells són positius.
// Alex Gonzalez Reinoso

public class Ex1_10NumPos {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int num = 0, cont = 0, conta = 0;
		boolean compro = false;
		while (!compro) {
			for (; conta <= 9;) {
				try {
					System.out.print("Escriu un numero enter: ");
					num = reader.nextInt();
					conta++;
					if (num > 0) {
						cont++;
					}
					compro = true;

				} catch (Exception e) {
					System.out.println("Nomes accepta numeros enters");
					reader.nextLine();
				}
			}
			System.out.println(cont);
			reader.close();
		}
	}
}