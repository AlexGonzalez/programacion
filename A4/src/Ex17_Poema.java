import java.util.Scanner; 

public class Ex17_Poema {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in); 
		int compFrase = 0;
		char continua = 'S';
		String frase;
		do {
			System.out.println("Escriu una frase  ");
			frase = reader.nextLine();
			compFrase++;
			System.out.println("Voleu introduir més frases (s/n)");
			continua = reader.next().charAt(0);
			while(continua!='s' && continua!='S' && continua!='n' && continua!='N') {
				System.out.println("La resposta ha de ser s o n. Voleu introduir més frases (s/n)");
				continua = reader.next().charAt(0);
			}
			reader.nextLine(); 
		} while (continua == 's' || continua == 'S');

		System.out.println("Heu introduit " + compFrase + " frases.");
		reader.close();

	} 
}