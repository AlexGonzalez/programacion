import java.util.Scanner;

public class Ex19_Rectabgle {

	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		try {
			System.out.println("Indica l'amplada: ");
			int amplada = reader.nextInt();
			System.out.println("Indica l'alçada: ");
			int alçada = reader.nextInt();
			int x;
			int y;
			if(amplada > 1 && alçada > 1) {
				for(y=0;y<alçada;y++) {
					for(x=0;x<amplada;x++) {
						if(x == 0 || x == amplada-1 || y == 0 || y == alçada-1) {
							System.out.print("*");
						}else {
							System.out.print(" ");
						}
					}
					System.out.println();
				}
			}else {
				System.out.println("Escriu uns números més grans.");
			}
		}catch (Exception e){
			System.out.println("Introdueix dades vàlides.");
		}
		reader.close();
	}
}