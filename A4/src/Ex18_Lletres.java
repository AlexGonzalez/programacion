import java.util.Scanner;

public class Ex18_Lletres {
	public static void main(String[] args) {
		String frase;
		int i = 0;
		int paraules = 0;
		Scanner reader = new Scanner(System.in);
		System.out.println("Introdueix una frase:\n");
		frase = reader.nextLine();
		if (frase.charAt(0) != ' ' && frase.charAt(0)!='.')
			paraules = 1;
		while (frase.charAt(i) != '.') {
			if (frase.charAt(i) == ' ' && frase.charAt(i + 1) != ' ' && frase.charAt(i+1)!='.') {
				paraules++;
			}
			i++;
		}
		System.out.println("Hi ha " + paraules + " paraules.");
		reader.close();
	}
}
