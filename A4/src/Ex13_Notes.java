import java.util.Scanner; 

public class Ex13_Notes {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in); 
		double nota = 0.0;
		double mitjana = 0;
		int excelent = 0, notable = 0, be = 0, sufi = 0, insu = 0, md = 0;
		boolean correcte = false;
		while (!correcte || nota != -1) {
			System.out.print("Introdueix una nota entre 0 i 10: ");
			try {
				nota = reader.nextDouble();
				if (nota >= 0 && nota <= 10) {
					correcte = true;
					mitjana = mitjana + nota;
					if (nota <= 2) {
						md++;
					} else if (nota < 5) {
						insu++;
					} else if (nota < 6) {
						sufi++;
					} else if (nota < 7) {
						be++;
					} else if (nota < 9) {
						notable++;
					} else {
						excelent++;
					}
				}else {
					System.out.println("Han de ser valors entre 0 i 10 inclosos  ");
				}
			} catch (Exception e) {
				System.out.println("Atenció! Només poden ser números  ");
				reader.nextLine();
			}
		}
		mitjana = mitjana / (md+insu+sufi+be+notable+excelent);
		System.out.println("La nota mitja de totes les notes es:" + mitjana);
		System.out.println("Excel.lents: " + excelent);
		System.out.println("Notables: " + notable);
		System.out.println("Bés: " + be);
		System.out.println("Suficients: " + sufi);
		System.out.println("Insuficients: " + insu);
		System.out.println("Molt Deficients: " + md);
		reader.close();
	} 
}