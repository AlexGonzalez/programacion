import java.util.Scanner;  

public class Ex14_Frase {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in); 	
		char lletra;
		int cont=0;
		int pos = 0;	
		System.out.println("Escriu una frase acabada en punt ");
		String input = reader.nextLine();  
		lletra = input.charAt(0);		
		while (lletra != '.')
		{
			if ((lletra == 'a') || (lletra == 'A'))
			{
				cont = cont + 1;
			}
			pos++;
			lletra = input.charAt(pos);
		}
		System.out.println("Heu introduit el caràcter A " + cont + " vegades");
		reader.close();
	}  
}
