import java.util.Scanner;

//
// Alex Gonzalez Reinoso

public class Ex6_10Numos {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int num = 0, contplus= 0, contminus= 0, conta = 0, cont = 0;
		boolean compro = false;
		while (!compro) {
			for (; conta <= 9;) {
				try {
					System.out.print("Escriu un numero enter: ");
					num = reader.nextInt();
					conta++;
					if (num > 0) {
						contplus++;
					} else if (num < 0) {
						contminus++;
					} else {
						cont++;
					}
					compro = true;

				} catch (Exception e) {
					System.out.println("Nomes accepta numeros enters");
					reader.nextLine();
				}
			}
			System.out.println(cont + " zeros, "+ contplus +" positius, " + contminus + " negatius");
			reader.close();
		}
	}
}

