import java.util.Scanner;

public class Ex8_Factorial {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int num = 0, factorial = 1;
		boolean compro = false;
		while (!compro) {
		try {
			System.out.print("Escriu un numero y farem el seu factorial: ");
			num = reader.nextInt();
			while (num < 0) {
				System.out.println("Ha de ser mes gran que 0");
				num = reader.nextInt();
			}
			for (int i = 1; i <= num; i ++) {
				factorial = factorial*i;
				compro = true;
			}
		
		}catch (Exception e) {
			System.out.println("Nomes accepta numero enters i positius");
			reader.nextLine();
		}
		}
		System.out.println("El numero factorial es: " + factorial);
	reader.close();	
	}
}
