import java.util.Scanner;
/*3.	Algorisme que llegeix un número enter N positiu i calcula la seva taula de multiplicar.
	Alex Gonzalez Reinoso
*/
public class Ex3_TaulaMultiplicar {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int num = 0, i = 0, res = 0;
		boolean compro = false;
		while (!compro) {
		try {
		System.out.print("Escriu un numero positiu i enter: ");
		num = reader.nextInt();
		while (num < 0) {
			System.out.println("Numero negatiu, torna a probar");
			num = reader.nextInt();
		}
		System.out.println("La Taula del " + num + " es:");
		while (i < 10) {
			i++;
			res = num * i;
			System.out.println(i + " * " + num + " = " + res);
		}
		compro = true;
		} catch (Exception e) {
			System.out.println("Nomes accepta numeros enters.");
			reader.nextLine();
		}
		}
		reader.close();
	}
}
