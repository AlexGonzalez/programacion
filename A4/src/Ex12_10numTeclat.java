import java.util.Scanner;

public class Ex12_10numTeclat {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in); 
		double num = 0.0;
		double major = 0.0;
		double menor = 0.0;
		double mitjana = 0;
		int comptador = 0;
		boolean correcte = false;
		while (!correcte) {
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextDouble();
				correcte = true;
				comptador = 1;
				mitjana = mitjana + num;
			} catch (Exception e) {
				System.out.println("Atenció! Han de ser números. ");
				reader.nextLine(); 
			}
		}
		major = num;
		menor = num;

		while (comptador < 10) {
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextDouble();
				mitjana = num + mitjana;
				if (num > major) {
					major = num;
				}
				if (num <= menor) {
					menor = num;
				}
				comptador++;
			} catch (Exception e) {
				System.out.println("Atenció! Han de ser números ");
				reader.nextLine(); 
			}
		}
		mitjana = mitjana / 10.0;
		System.out.println("El numero més gran és " + major + " el més petit és " + menor
				+ " i la mitjana aritmètica és " + mitjana);
		reader.close();
	}
}