import java.util.Scanner;

public class Ex4_Notes {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int num = 0,nota = 0;
		boolean compro = false;
		while (!compro) {
			try {
				System.out.println("Escriu la nota entre 1 i 10 (-1 per acabar)");
				num = reader.nextInt();
				while (num < -1 || num > 10) {
					System.out.println("El numero ha d'estar entre 1 i 10:");
				}
				switch (num){
				case 10:
					nota++;
					break;
				case -1:	
					compro = true;
					break;
				}
			} catch (Exception e) {
				System.out.println("Nomes accepta numeros enters");
				reader.nextLine();
			}
		}
		System.out.println("Hi han " + nota + " 10.");
		
		
	reader.close();
	}
	
}
