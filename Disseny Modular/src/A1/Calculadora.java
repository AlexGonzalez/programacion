package A1;

import java.util.Scanner;

public class Calculadora {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		char opcio =' ';
		int resultat = 0;
		while (opcio != '0') {
			menu();
			opcio = demanaropcio();
			resultat = calculadora(opcio);
			System.out.println(resultat);
		}
		reader.close();
	}
	static void menu() {
		System.out.println("\tCalculadora");
		System.out.println("\t+.Suma");
		System.out.println("\t-.Resta");
		System.out.println("\t*.Multiplicacio");
		System.out.println("\t/.Divisio");
		System.out.println("\t0.Sortir");
		System.out.println("_______________________________________");
		System.out.print("\tEscull una opcio: ");
	}
	static char demanaropcio() {
		Scanner reader = new Scanner(System.in);
		char opcio = ' ';
		opcio = reader.next().charAt(0);
		reader.close();
		return	(char) opcio;
	}
	static int calculadora(char operacio) {
		Scanner reader = new Scanner(System.in);
		int num = 0, num1 = 0, resultat = 0;
		System.out.print("Numero 1:");
		num = reader.nextInt();
		System.out.print("Numero 2:");
		num1 = reader.nextInt();
		switch (operacio) {
		case '+':
			resultat = num + num1;
			break;
		case '-':
			resultat = num - num1;
			break;
		case '*':
			resultat = num * num1;
			break;
		case '/':
			resultat = num / num1;
			break;
		default:
			System.out.println("Opcio incorrecte");
			break;
		}
		reader.close();
		return (int) resultat;
	}
}


