package A2;

import java.util.Scanner;

import java.util.Arrays;

public class Penjat {
	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		int opcion = -1, numpalabras = 0, vidas = 5;
		String palabra;
		String nombre = " ";
		boolean vida = false, numpal = false, nombrecase = false;
		palabra = palabraRND();
		char[] adivinar = new char[palabra.length()];
		char[] comprobar = new char[palabra.length()];
		adivinar = PalabraAdivinar(palabra);
		while (opcion != 0) {
			menu();
			opcion = pediropcion();
			switch (opcion) {	
			case 1:
				ayuda();
				break;
			case 2:
				nombre = nombre();
				nombrecase = true;
				break;
			case 3:
				if (nombrecase) {
					System.out.println("Hola " + nombre
							+ " suerte y que empiece el juego, si pones un 0 acabaras la partida y tendras que empezar una nueva!");
					char letra = ' ';
					for (int i = 0; i < adivinar.length; i++) {
						System.out.print(adivinar[i] + " ");
					}
					while (!vida && !numpal && letra != '0') {

						for (int x = 0; x < adivinar.length; x++) {
							comprobar[x] = adivinar[x];
						}
						letra = LeerLetra();
						adivinar = Comprobar(adivinar, letra, palabra);
						for (int i = 0; i < adivinar.length; i++) {
							System.out.print(adivinar[i] + " ");
						}
						numpalabras = victoria(adivinar);

						vidas = vidas(adivinar, comprobar, vidas);
						System.out.println(" \nVidas:" + vidas);
						if (vidas == 0) {
							vida = true;
							opcion = 0;
						}
						if (numpalabras == palabra.length()) {
							numpal = true;
							opcion = 0;
						}
					}
				} else {
					System.out.println("Tienes que definir el jugador primero");
				}
				break;
			default:
				System.out.println(" Opcion incorrecta");
			}
		}
		if (vida == true) {
			System.out.println("Has perdido");
			System.out.println("La palabra secreta era: " + palabra);
			
		} else if (numpal == true) {
			System.out.println("Felicidades has ganado " + nombre);
		} else {
			System.out.println("Hasta pronto");
		}
		System.exit(0);
	}

	static void menu() {
		System.out.println("\tCOLGADO JUEGOS");
		System.out.println("\t1.Mostrar ayuda");
		System.out.println("\t2.Definir jugador");
		System.out.println("\t3.Jugar partida");
		System.out.println("\t0.Salir");
		System.out.println("_______________________________________");
		System.out.print("\tEscoge una opcion: ");
	}

	static int pediropcion() {
		int opcion = -1;
		try {
			opcion = reader.nextInt();
			while (opcion < 0 || opcion > 6) {
				System.out.println("Opcion incorrecta");
				menu();
				opcion = reader.nextInt();
			}
		} catch (Exception e) {
			System.out.println("Solo acepta numeros, vuelve a probar: ");
			reader.nextLine();
		}
		return (int) opcion;
	}

	static String nombre() {
		String nom;
		System.out.print("Como te llamas ?");
		nom = reader.next();
		return (String) nom;
	}

	static void ayuda() {

		System.out.println("****  Instrucciones juego del Ahorcado  ******");
		System.out.println("----------------------------------------------");
		System.out.println(
				"El juego consiste en adivinar una palabra secreta seleccionada por el programa de forma aleatoria.");
		System.out.println(
				"Al comienzo del juego, el programa muestra una sucesión de guiones. Cada guión contiene una letra de la palabra que se ha de adivinar.");
		System.out.println(
				"En cada tirada el jugador indica una letra. Si la letra está dentro de la palabra, se muestra, tantas veces como aparezca, sustituyendo el guión correspondiente por la posición que ocupa.");
		System.out.println(
				"Si la letra indicada por el jugador no está en la palabra, el jugador tendrá un fallo. Se pierde la partida, si se falla más de 10 veces; y se gana si se adivina completamente la palabra sin superar los 10 fallos.");
		System.out.println("Mucha suerte y a jugar.... \n\n");
	}

	static String palabraRND() {
		String palabra;
		String palabrarnd[] = { "SEKIRO", "MINECRAFT", "PHASMAPHOBIA", "OVERCOOKED", "POKEMON" };
		palabra = palabrarnd[(int) (Math.random() * 5)];
		return (String) palabra;
	}

	static char[] PalabraAdivinar(String palabra) {
		char[] adivinar = new char[palabra.length()];
		adivinar = palabra.toCharArray();
		for (int i = 0; i < adivinar.length; i++) {
			adivinar[i] = '_';
		}
		return (char[]) adivinar;
	}

	static char LeerLetra() {
		char letra = ' ';
		System.out.println("\nDi una letra");
		letra = reader.next().charAt(0);
		return (char) letra;
	}

	static char[] Comprobar(char[] adivinar, char letra, String palabra) {
		char[] compro = palabra.toCharArray();
		letra = Character.toUpperCase(letra);

		for (int i = 0; i < adivinar.length; i++) {
			if (compro[i] == letra) {
				adivinar[i] = letra;
			}
		}
		return (char[]) adivinar;
	}

	static int victoria(char[] adivinar) {
		int num = 0;
		num = 0;
		for (int i = 0; i < adivinar.length; i++) {
			if (adivinar[i] != '_') {
				num++;
			}
		}

		return (int) num;
	}

	static int vidas(char[] adivinar, char[] comprobar, int vidas) {

		boolean compro = Arrays.equals(comprobar, adivinar);
		if (compro) {
			vidas--;
		}

		return (int) vidas;
	}
}
