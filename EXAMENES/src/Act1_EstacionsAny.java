import java.util.Scanner;

public class Act1_EstacionsAny {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int mes = 0, dia = 0;
		System.out.print("Escriu el mes (En numero): ");
		mes = reader.nextInt();
		while (mes < 1 || mes > 12) {
			System.out.print("Mes no valid ha d'estar entre 1 o 12, torna a esciure: ");
			mes = reader.nextInt();
		}
		System.out.print("Escriu el dia (En nummero): ");
		dia = reader.nextInt();
		while (dia < 1 || dia > 31) {
			System.out.print("Dia no valid ha d'estar entre 1 o 12, torna a esciure: ");
			dia = reader.nextInt();
		}
		switch (mes) {
		case 1:
		case 2:
		case 3:
			if (mes == 3 && dia >= 20) {
				System.out.println("Primavera");
			} else {
				System.out.println("Hivern");
			}
			break;
		case 4:
		case 5:
		case 6:
			if (mes == 6 && dia >= 22) {
				System.out.println("Estiu");
			} else {
				System.out.println("Primavera");
			}
			break;
		case 7:
		case 8:
		case 9:
			if (mes == 9 && dia >= 22) {
				System.out.println("Tardor");
			} else {
				System.out.println("Estiu");
			}
			break;
		case 10:
		case 11:
		case 12:
			if (mes == 12 && dia >= 21) {
				System.out.println("Hivern");
			} else {
				System.out.println("Tardor");
			}
			break;
		default:
			break;
		}
		reader.close();
	}
}
