import java.util.Scanner;

public class Endevina {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int valor = 0, num = 0, resta = 0, comp = 0;
		boolean compro = false;
		valor = (int) (Math.random() * 20+1);

		System.out.print("Adivina el numero esta entre 1-20 (inclosos).\n");
		while (!compro) {
			System.out.println("Escriu un numero enter: ");
			try {
				num = reader.nextInt();
				if (num > 0 && num < 20) {
					resta = valor - num;
					comp++;
					if (resta == 0) {
						System.out.println("Moltbe l'has encertat :)");
						compro = true;
					} else if (resta >= -3 && resta <= 3) {
						System.out.println("Calent, calent..Torna a provar !");
					} else if (resta >= -5 && resta <= 5) {
						System.out.println("Molt fred… torna a provar!");
					} else {
						System.out.println("No és aquest! Torna a provar!");
					}
				} else {
					System.out.println("Nomes accepta numeros entre 1 i 20 inclosos");
				}
			} catch (Exception e) {
				System.out.println("Nomes accepta numero enters y positius.");
				reader.nextLine();
			}
		}
		System.out.println("Has necesitat "+ comp + " intents" );
		reader.close();
	}
}

