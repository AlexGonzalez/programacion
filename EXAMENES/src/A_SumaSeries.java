import java.util.Scanner;

public class A_SumaSeries {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int tamañ = 0, sumas = 0, j = 0, cont = 1;

		try {
			System.out.print("De quin tamañ vols el vector ?: ");
			tamañ = reader.nextInt();
			while (tamañ < 0) {
				System.out.print("Ha de ser positiu, torna a provar: ");
				tamañ = reader.nextInt();
			}
			int[] serie = new int[tamañ];
			int[] suma = new int[tamañ-1];
			System.out.print("Escriu la serie: ");
			for (int i = 0; i < serie.length; i++) {
				serie[i] = reader.nextInt();
			}
			for (int i = 0; i < serie.length-1; i++) {
				for (j = cont; j <= suma.length; j++) {
					sumas = serie[j] + sumas;
				}	
				cont++;
				suma[i] = sumas;
				sumas = 0;
				
			}
			System.out.print("Serie Inicial : ");
			for (int i = 0; i < serie.length; i++) {
				System.out.print(serie[i] + " ");
			}
			System.out.print("\nSerie resultat : ");
			for (int i = 0; i < suma.length; i++) {
				System.out.print(suma[i] + " ");
			}
			
		} catch (Exception e) {
			System.out.println("Ha de ser un numero enter");
		}
		reader.close();
	}
}
