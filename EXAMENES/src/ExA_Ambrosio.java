import java.util.Scanner;

public class ExA_Ambrosio {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int altura = 0, bombons = 0;
		boolean compro = false;

		while (!compro) {
			try {
				System.out.print("De quant pisos vols la teva piramide: ");
				altura = reader.nextInt();
				while (altura <= 0) {
					System.out.println("Ha de ser positiu, torna a probar: ");
					altura = reader.nextInt();
				}
				for (int i = altura; i >= 1; i--) {
					bombons = bombons + altura * altura;
					altura = altura - 1;
				}
				compro = true;
			} catch (Exception e) {
				System.out.println("Nomes accepta numero enters i positius.");
				reader.nextLine();
			}
		}
		System.out.println(bombons);
		reader.close();
	}
}
