import java.util.Scanner;

public class Act4_Restriccions {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		char ccaa;
		double uci = 0, contagis = 0, pcr = 0;
		System.out.print("En quina comunitat Autonoma vius: ");
		ccaa = reader.next().charAt(0);
		while (ccaa != 'M' && ccaa != 'A' && ccaa != 'C') { // Comprovació de comunitats autonomes
			System.out.print("Error Comunitat Autonoma, torna a probar: ");
			ccaa = reader.next().charAt(0);
		}
		System.out.print("Quin numero real en % tens de llits ocupats a la UCI: ");
		uci = reader.nextDouble();
		while (uci < 0 || uci > 100) { // Comprovaci� de percentatge
			System.out.print("Numero incorrecte trona a probar: ");
			uci = reader.nextDouble();
		}
		System.out.print("Quants contagis tens per cada 100000 habitants: ");
		contagis = reader.nextDouble();
		while (contagis < 0 || contagis > 100000) { // Comprovaci� de contagis
			System.out.print("Numero incorrecte trona a probar: ");
			contagis = reader.nextDouble();
		}

		System.out.print("Quin numero real en % tens de positius en PCR: ");
		pcr = reader.nextDouble();
		while (pcr < 0 || pcr > 100) { // Comprovaci� de percentatge
			System.out.print("Numero incorrecte trona a probar: ");
			pcr = reader.nextDouble();
		}
		System.out.println("Portar mascareta");

		if (uci > 30 && contagis > 500 && pcr > 10) {
			System.out.println("Estat d'alarma");
		} else {
			switch (ccaa) {
			case 'M':
				if (uci >= 20) {
					System.out.println("Tacament perimetral");
					if (contagis > 250) {
						System.out.println("Bars tancats");
					}
				}
				break;
			case 'C':
				if (pcr > 5) {
					System.out.println("Tancaran els espectacles");
				}
				break;
			case 'A':
				if (uci > 30 && contagis > 400 && pcr > 15) {
					System.out.println("Tancaran les escoles");
				break;
				}
			}
		}
		reader.close();
	}
}
