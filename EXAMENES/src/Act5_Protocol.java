import java.util.Scanner;

public class Act5_Protocol {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		char modalitat, grau, torn; // Modalitat (Batxillerat,ESO,Cicles) Grau (Superior,Mitja) Tron (Mati,Tarda)
		int curs = 0;
		System.out.println("Hola, quina modalitat estas cursant (E).Eso (B).Batxillerat (C).Cicles (Nomes accepta majuscules): ");
		modalitat = reader.next().charAt(0);

		while (modalitat != 'E' && modalitat != 'B' && modalitat != 'C') { // Comprovació de si ha escollit una modalitat correcta
			System.out.println("Aquesta modalitat no existeix");
			modalitat = reader.next().charAt(0);
		}
		switch (modalitat) {
		case 'E':
			System.out.println("Quin curs estas fent: ");
			curs = reader.nextInt();
			while (curs < 1 || curs > 4) { // Comprovació de si ha escollit un curs correcte
				System.out.println("La ESO nomes te 4 cursos");
				curs = reader.nextInt();
			}
			switch (curs) {
			case 1:
				System.out.println("Entres per Juvenal a les 8:00");
				break;
			case 2:
				System.out.println("Entres per Juvenal a les 9:00");
				break;
			case 3:
				System.out.println("Entres per St.Isidor a les 9:00");
				break;
			case 4:
				System.out.println("Entres per St.Isidor a les 9:00");
				break;
			}
			break;
		case 'B':
			System.out.println("Quin curs estas fent: ");
			curs = reader.nextInt();
			while (curs < 1 || curs > 2) { // Comprovació de si ha escollit un curs correcte
				System.out.println("Batxillerat nomes te 2 cursos");
				curs = reader.nextInt();
			}
			switch (curs) {
			case 1:
				System.out.println("Entres per St.Isidor a les 8:00");
				break;
			case 2:
				System.out.println("Entres per St.Isidor a les 9:00");
				break;
			}
			break;
		case 'C':
			System.out.println("Quin grau estas fent (S).Superior (M).Mitja (Nomes accepta en majuscules): ");
			grau = reader.next().charAt(0);
			while (grau != 'S' && grau != 'M') {// Comprovació de si ha escollit un grau correcte
				System.out.println("Aquest grau no existeix, torna a probar: ");
				grau = reader.next().charAt(0);
			}
			System.out.println("Vas per el (M).Mati o (T).Tarda (Nomes accepta en majuscules): ");
			torn = reader.next().charAt(0);
			while (torn != 'T' && torn != 'M') {// Comprovació de si ha escollit un torn correcte
				System.out.println("Aquest torn no existeix, tornar a probar: ");
				torn = reader.next().charAt(0);
			}
			switch (grau) {
			case 'S':
				/*System.out.println("Vas per el (M).Mati o (T).Tarda (Nomes accepta en majuscules): ");
				torn = reader.next().charAt(0);
				while (torn != 'T' && torn != 'M') {// Comprovació de si ha escollit un torn correcte
					System.out.println("Aquest torn no existeix, tornar a probar: ");
					torn = reader.next().charAt(0);
				}*/
				switch (torn) {
				case 'M':
					System.out.println("Entres per St.Isidor a les 9:00");
					break;
				case 'T':
					System.out.println("Entres per St.Isidor a les 16:00");
					break;
				}
				break;
			case 'M':
				/*System.out.println("Vas per el (M).Mati o (T).Tarda (Nomes accepta en majuscules): ");
				torn = reader.next().charAt(0);
				while (torn != 'T' && torn != 'M') { //  Comprovació de si ha escollit un torn correcte
					System.out.println("Aquest torn no existeix, tornar a probar: ");
					torn = reader.next().charAt(0);
				}*/
				switch (torn) {
				case 'M':
					System.out.println("Entres per St.Isidor a les 8:00");
					break;
				case 'T':
					System.out.println("Entres per St.Isidor a les 15:00");
					break;
				}
				break;
			}
			break;
		}
		reader.close();
	}
}
