import java.util.Scanner;


public class B_Fibo {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int tamañ = 0, fibo = 1, fibo1 = 1, num = 0, cont = 0, par = 0, conta = 0;

		try {
			System.out.print("Quants numeros de Fibonacci vols veure? :");
			tamañ = reader.nextInt();
			while (tamañ < 2) {
				System.out.println(" Ha de ser mes gran que 2, torna a provar: ");
				tamañ = reader.nextInt();
			}
			int[] Fibonacci = new int[tamañ];
			Fibonacci[0] = 1;
			Fibonacci[1] = 1;
			for (int i = 2; i < Fibonacci.length; i++) {
				num = fibo + fibo1;
				Fibonacci[i] = num;
				fibo = fibo1;
				fibo1 = num;
				if (num % 2 == 0) {
					cont++;
				}
			}
			int parells[] = new int[cont];
			for (int i = 0; i < Fibonacci.length; i++) {
				par = Fibonacci[i];
				if (par % 2 == 0) {
					parells[conta] = par;
					conta++;
				}
			}
			System.out.print("Els " + tamañ + " primers elemets de Fibonacci son : ");
			for (int i = 0; i < Fibonacci.length - 1; i++) {
				System.out.print(Fibonacci[i] + ", ");
			}
			System.out.print(Fibonacci[tamañ - 1] + "\n");
			System.out.print("Els elements parells continguts serien {");
			for (int i = 0; i < parells.length - 1; i++) {
				System.out.print(parells[i] + ", ");
			}
			System.out.print(parells[conta - 1] + "}");

		} catch (Exception e) {
			System.out.println("Ha de ser un numero enter");
		}
		reader.close();
	}
}