import java.util.Scanner;

public class Act3_Hora {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int hora = 0, minuts = 0;
		String[] dia = new String[1]; // Per guardar MATI / NIT / TARDA / MATINADA
		System.out.print("Escriu l'hora actual: ");
		hora = reader.nextInt();
		while (hora < 0 || hora > 23) {
			System.out.print("Hora incorrecte torna a probar: ");
			hora = reader.nextInt();
		}
		System.out.print("Escriu els minuts actuals: ");
		minuts = reader.nextInt();
		while (minuts < 0 || minuts > 59) {
			System.out.print("Minuts incorrecte torna a probar: ");
			minuts = reader.nextInt();
		}
		switch (hora) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			if (hora == 6) {
				if (minuts == 0) {
					hora = 24 - hora;
					minuts = 00;
					dia[0] = "MATINADA";
				} else {
					hora = 23 - hora;
					minuts = 60 - minuts;
					dia[0] = "MATI";
				}
			} else {
				hora = 23 - hora;
				minuts = 60 - minuts;
				dia[0] = "MATINADA";

			}

			break;
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
			if (hora == 14) {
				if (minuts == 0) {
					hora = 24 - hora;
					minuts = 0;
					dia[0] = "MATI";
				} else {
					hora = 23 - hora;
					minuts = 60 - minuts;
					dia[0] = "TARDA";
				}
			} else {
				hora = 23 - hora;
				minuts = 60 - minuts;
				dia[0] = "MATI";

			}
			break;
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
			if (hora == 20) {
				if (minuts == 0) {
					hora = 24 - hora;
					minuts = 0;
					dia[0] = "TARDA";
				} else {
					hora = 23 - hora;
					minuts = 60 - minuts;
					dia[0] = "NIT";
				}
			} else {
				hora = 23 - hora;
				minuts = 60 - minuts;
				dia[0] = "TARDA";

			}
			break;
		case 21:
		case 22:
		case 23:
			hora = 23 - hora;
			minuts = 60 - minuts;
			dia[0] = "NIT";
			break;
		}	
		if (minuts == 60) {
				minuts = 0;
				hora = hora + 1;
			} 
				if (hora < 10 && minuts < 10) {
					System.out.println(dia[0] + " Per final del dia " + "0" + hora + ":0" + minuts);
				} else if (hora < 10) {
					System.out.println(dia[0] + " Per final del dia " + "0" + hora + ":" + minuts);
				} else if (minuts < 10) {
					System.out.println(dia[0] + " Per final del dia " + hora + ":0" + minuts);
				} else {
					System.out.println(dia[0] + " Per final del dia " + hora + ":" + minuts);
				}
			
			reader.close();
		}
	}

