import java.util.Scanner;

public class ExC_TemperaturesCOVID {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int grups = 0, alumnes = 0, grup = 1, i = 0, j = 0;
		double temperatura = 0, sumtemp = 0, temp1 = 0, tempmax = 0, tempmin = 80;
		boolean grupsok = false, alumnesok = false;
		while (!grupsok) {
			try {
				System.out.print("Introdueix el nombre de grups que entrara avui: ");
				grups = reader.nextInt();
				while (grups <= 0) {
					System.out.print("Els grups han de ser mes gran que 0: ");
					grups = reader.nextInt();
				}
				while (!alumnesok) {
								
						for (j = 1; j <= grups; j++) {
							try {
							System.out.print("Quants alumnes hi ha al " + grup + " grup: ");					
							alumnes = reader.nextInt();	
							while (alumnes <= 0) {
								System.out.println("Els alumnes han de ser mes gran que 0: ");
								alumnes = reader.nextInt();
							}
							for (i = 1; i <= alumnes; i++) {								
								System.out.print("Escriu temperatura de l'alumne " + i + " del grup " + grup + " :");
								temperatura = reader.nextDouble();
								while (temperatura <= 0) {
									System.out.println("No pot ser 0 o negativa, torna a provar: ");
									temperatura = reader.nextDouble();
								}
								if (temperatura > 37.5) {
									System.out.println("ALARAMA, aquest no hi entra");
								}
								temp1 = temperatura;
								if (alumnes == 1) {
									tempmax = temp1;
									tempmin = temp1;
								} else if (temp1 < tempmin) {
									tempmin = temp1;
								} else if (temp1 > tempmax) {
									tempmax = temp1;
								}
								sumtemp = sumtemp + temperatura;
							}
							sumtemp = sumtemp / alumnes;
							System.out.println("La temperatura mitjana del grup " + grup + " es: " + sumtemp);
							sumtemp = 0;
							grup++;
							grupsok = true;
							if (j == grups) {
							alumnesok = true;
							}												
								
						}catch (Exception e) {
							System.out.println("Els alumnes han de ser un numero positiu.");
							reader.nextLine();
						}
					} 
				}
			} catch (Exception e) {
				System.out.println("Els grups han de ser un valor enter i positiu");
				reader.nextLine();
			}
		}
		System.out.println("La temperatura minima es: " + tempmin);
		System.out.println("La temperatura maxima es: " + tempmax);
		reader.close();
	}
}
