import java.util.Scanner;

public class ExB_Fibonacci {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		long  num = 1, num1 = 1, fibo = 1; // long per que si demanes que es mostrin molts numeros
		boolean compro = false;	          // un int no els pot guardar i surten negatius
		int enters = 0, cont = 0;
		
		while (!compro) {
			try {
				System.out.println(
						"Quants numeros de la series de Fibnacci enters positius vols mostrar que siguin multiples de 5: ");
				enters = reader.nextInt();
				while (enters <= 0) {
					System.out.println("El valor ha de ser mes gran que 0");
					enters = reader.nextInt();
				}
				while (cont != enters) {
					fibo = num + num1;
					num1 = fibo;
					fibo = num1 + num;
					num = fibo;
					if (fibo % 5 == 0) {
						cont++;
						if (enters == 1) {
							System.out.println(fibo);
						} else if (cont == enters) {
							System.out.println("i " + fibo);
						} else if (cont == enters - 1) {
							System.out.print(fibo + " ");
						} else {
							System.out.print(fibo + ", ");
						}
					}
					num1 = fibo;
					fibo = num1 + num;
					num = fibo;
					if (fibo % 5 == 0) {
						cont++;
						if (enters == 1) {
							System.out.println(fibo);
						} else if (cont == enters) {
							System.out.println("i " + fibo);
						} else if (cont == enters - 1) {
							System.out.print(fibo + " ");
						} else {
							System.out.print(fibo + ", ");
						}
					}
					num = fibo;
				}
				compro = true;
			} catch (Exception e) {
				System.out.println("Nomes accepta numeros positius y enters");
				reader.nextLine();
			}
		}
		reader.close();
	}

}
