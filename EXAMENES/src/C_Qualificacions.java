import java.util.Scanner;

public class C_Qualificacions {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int alumnes = 0, cont = 0;
		double nota = 0, nota1 = 0, notaf = 0;
		try {
			System.out.println("Quants alumnes hi han: ");
			alumnes = reader.nextInt();
			while (alumnes < 0) {
				System.out.println("Han de ser un numero major que 0, torna a provar: ");
				alumnes = reader.nextInt();
			}

		} catch (Exception e) {
			System.out.println("Els alumnes han de ser un numero enter y positiu");
		}

		double notes[] = new double[alumnes * 2];
		String nom[] = new String[alumnes];
		for (int i = 0; i < nom.length; i++) {
			System.out.println("Escriu el nom de l'alumne " + (i + 1) + " :");
			nom[i] = reader.next();
		}
		try {
			for (int i = 0; i < nom.length; i++) {
				for (int j = 0; j < 2; j++) {
					System.out.println("Escriu la nota del " + (j + 1) + " examen de " + nom[i]);
					nota = reader.nextDouble();
					if (nota >= 0 && nota <= 10) {
						notes[cont] = nota;
						cont++;
					} else {
						System.out.println("Nota invalida");
						System.exit(j);						
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Ha de ser un numero enter y positiu la nota");
		}
		System.out.print("Notes primer examen (per comprovar): ");
		for (int i = 0; i < notes.length; i = i + 2) {
			System.out.print(notes[i] + " ");
		}
		System.out.print("\nNotes segon examen (per comprovar): ");
		for (int i = 1; i < notes.length; i = i + 2) {
			System.out.print(notes[i] + " ");
		}
		cont = 0;
		double notesfinals[] = new double[alumnes];
		for (int i = 0; i < notes.length; i = i + 2) {
			for (int j = cont; j < 1; j++) {
				nota = notes[j];
				nota1 = notes[j + 1];
				cont++;

				notaf = nota * 0.3 + nota1 * 0.7;
				if (nota1 > nota) {
					notaf = nota * 0.1 + nota1 * 0.9;
				}
				if (nota1 < 2 || nota < 2) {
					if (notaf > 4) {
						notaf = 4;
					}
				}
				notesfinals[i] = notaf;
			}
		}
		System.out.println(" \nLlista de Qualifiacions de clase: ");
		for (int i = 0; i < nom.length; i++) {
			System.out.println(nom[i] + " : " + notesfinals[i]);

		}
		reader.close();
	}
}
