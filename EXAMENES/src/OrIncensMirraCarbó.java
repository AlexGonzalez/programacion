import java.util.Scanner;

public class OrIncensMirraCarbó {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int nota = 0, incenscont = 0, mitjana = 0;
		boolean mirra = false, carbo = false;

		for (int i = 0; i <= 9; i++) {
			try {

				nota = reader.nextInt();
				if (nota > 0 && nota < 10) {
					if (nota < 5) {
						carbo = true;
					} else {
						if (nota >= 6) {
							incenscont++;
							if (nota == 10) {
								mirra = true;
							}
						}
					}
					mitjana = mitjana + nota;
				}
				mitjana = mitjana / 10;
				if (carbo) {
					System.out.println("Carbo");
				} else if (incenscont >= 3) {
					System.out.println("Incens");

					if (mirra) {
						System.out.println("Mirra");
					}
					if (mitjana > 8) {
						System.out.println("Or");
					}
				} else {
					System.out.println("Nomes accepta numeros entre 1 i 20 inclosos");
				}
			} catch (Exception e) {
				System.out.println("Nomes accepta numero enters y positius.");
				reader.nextLine();
			}
		}
			
		
		reader.close();
	}
}
