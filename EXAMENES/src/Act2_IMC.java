import java.util.Scanner;

public class Act2_IMC {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		double pes = 0, altura = 0, imc = 0;
		System.out.println("Escriu el teu pes en KG: ");
		pes = reader.nextDouble();
		System.out.println("Escriu la teva altura en m: ");
		altura = reader.nextDouble();

		imc = pes / Math.pow(altura, 2);

		if (imc <= 18) {
			System.out.println("Pes per sota de la normalitat");
		} else if (imc <= 25) {
			System.out.println("Pes adecuat");
		} else if (imc <= 30) {
			System.out.println("Sobrepes");
		} else {
			System.out.println("Obesitat");
		}
		reader.close();
	}
}
