import java.util.Scanner;

public class Act3_DAM {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int via = 0, cas = 0, modbatx = 0;
		double nota = 0;
		boolean cags = true;
		System.out.print(
				"Hola, comprovarem si podras entrar a DAM, escriu per la via d'acces que entraries (1).Batxillerat (2).CFGM 3.(Prova d'acces): ");
		via = reader.nextInt();
		while (via > 3 || via < 1) {
			System.out.print("Comprova la via d'acces (1).Batxillerat (2).CFGM 3.(Prova d'acces): ");
			via = reader.nextInt();
		}
		switch (via) {
		case 1:
			System.out.print("Quina modalitat has fet (5).Artistic (6).Cientific-Tecnologic (7).Humanistic-Social: ");
			modbatx = reader.nextInt();
			while (modbatx < 5 || modbatx > 7) {
				System.out.print("Quina modalitat has fet (5).Artistic (6).Cientific-Tecnologic (7).Humanistic-Social: ");
				modbatx = reader.nextInt();
			}
			System.out.print("Escriu la teva nota: ");
			nota = reader.nextDouble();
			if (nota > 9) {
				System.out.println("Segur que hi entres");
			} else if (nota > 5) {
				if (modbatx == 6) {
					System.out.println("Tens un 90% d'entrar");
				} else {
					System.out.println("Tens un 60% d'entrar");
				}
			} else if (nota < 5) {
				System.out.println("Nota suspesa");
			} else {
				System.out.println("Dades incorrectes");
				System.exit(via);
			}
			break;
		case 2:
			System.out.print("Has fet el curs d'acces de GS (1).Si (2).No: ");
			cas = reader.nextInt();
			if (cas == 1) {
				cags = true;
			} else if (cas == 2) {
				cags = false;
			} else {
				System.out.println("Dades incorrectes");
				System.exit(via);
			}
			System.out.print("Escriu la teva nota: ");
			nota = reader.nextDouble();
			if (nota > 9) {
				System.out.println("Segur que hi entres");
			} else if (nota > 5) {
				if (cags == true) {
					System.out.println("Tens un 45% d'entrar");
				} else {
					System.out.println("Tengos un 20% d'entrar");
				}
			} else if (nota < 5) {
				System.out.println("Nota suspesa");
			} else {
				System.out.println("Dades incorrectes");
				System.exit(via);
			}
			break;
		case 3:
			System.out.print("Escriu la teva nota: ");
			nota = reader.nextDouble();
			if (nota > 9) {
				System.out.println("Segur que hi entres");
			} else if (nota < 5) {
				System.out.println("Nota suspesa");
			} else {
				System.out.println("Dades incorrectes");
				System.exit(via);
			}
			break;
		default:
			System.out.println("Dades incorrectes");
		}
		reader.close();
	}

}
